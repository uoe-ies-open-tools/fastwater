# -*- coding: utf-8 -*-
"""
Geometry functions for manipulating vector field data.
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import numpy as np
# Non-Standard Python Dependencies
# Local Module Dependencies
# Other Dependencies

#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

# Coordinate Functions

def pol2cart(r, theta):
    """
    Coordinate conversion: Polar to Cartesian
    
    Parameters
    ----------
    r : numpy.array, float 
        radial distance
    theta : numpy.array, float 
        angle in degrees

    Returns
    -------
    x : numpy.array, float
        cartesian x coordinate
    y : numpy.array, float
        cartesian y coordinate
    
    """
    x = r * np.cos(np.deg2rad(theta,dtype='double'),dtype='double')
    y = r * np.sin(np.deg2rad(theta,dtype='double'),dtype='double')
    return x,y


def cart2pol(x, y):
    """
    Coordinate conversion: Cartesian to Polar
    
    Parameters
    ----------
    x : numpy.array, float
        cartesian x coordindate
    y : numpy.array, float
        cartesian y coordinate
    
    Returns
    -------
    r : numpy.array, float
        polar cordinate radial distance
    theta : numpy.array, float
        polar cordinate angle (in degrees)
    
    """
    r = np.sqrt(np.power(x,2.0,dtype='double') + 
                np.power(y,2.0,dtype='double'),dtype='double')
    theta = np.rad2deg(np.arctan2(y,x),dtype='double')
    return r, theta


def pol2cmplx(radii, angles, angularUnits=0):
    """
    Coordinate conversion: Polar to Complex
    
    Parameters
    ----------
    radii : numpy.array, float
        polar coordinate radial distances
    angles : numpy.array, float 
        polar cordinate angles
    anglularUnits : int
        angular units flag  (0 = degress [default], 1 = radians)

    Returns
    -------
    cmplx : numpy.array, complex
        complex numbers (x + jy)

    """
    if angularUnits == 0:
        cmplx = radii * np.exp(1j*angles)
    else:
        cmplx = radii * np.exp(1j*np.deg2rad(angles))
    return cmplx


def cmplx2pol(x, angularUnits=0):
    """
    Coordinate conversion: Complex to Polar
    
    Parameters
    ----------
    x : numpy.array, complex
        complex numbers (x + jy)
    anglularUnits: int
        angular units flag (0 = degress [default], 1 = radians)

    Returns
    -------
    radii : numpy.array, float
        polar coordinate radial values
    angles : numpy.array, float
        polar coordinate angle values

    """
    if angularUnits == 0:
        radii = abs(x)
        angles = np.angle(x)
    else:
        radii = abs(x)
        angles = np.rad2deg(np.angle(x))
    return radii, angles


def cart2cmplx(x, y):
    """
    Coordinate conversion: Cartesian to Complex
    
    Parameters
    ----------
    x : numpy.array, float
        cartesian x-coordinate
    y : numpy.array, float
        cartesian y coordinate
    
    Returns
    -------
    cmplx : numpy.array, complex
        complex numbers (x + jy)

    """
    cmplx = x + 1j*y
    return cmplx


def cmplx2cart(c):
    """
    Coordinate conversion: Complex to Cartesian
    
    Parameters
    ----------
    c : numpy.array, complex
        complex numbers (x + jy)
    
    Returns
    -------
    x : numpy.array, float
        cartesian x coordinate
    y : numpy.array, float
        cartesian y cordinate
    
    """
    x = np.real(c)
    y = np.imag(c)
    return x, y


def rotateVectorField(U,V,W,Theta):
    """
    Rotate a set of vector field data about the z axis of the coordinate 
    system.

    Parameters
    ----------
    U : numpy.array, float
        array of vector field values along the x axis.
    V : numpy.array, float
        array of vector field values along the y axis.
    W : numpy.array, float
        array of vector field values along the z axis.
    Theta : float
        rotation angle in degrees (including sign for rotation direction).

    Returns
    -------
    Ur : numpy.array, float
        array of vector field values along the x axis in rotated coordinates.
    Vr : numpy.array, float
        array of vector field values along the y axis in rotated coordinates.
    Wr : numpy.array, float
        array of vector field values along the z axis in rotated coordinates.

    """
    thetaRad = np.deg2rad(Theta)
    Rz = np.asarray([[np.cos(thetaRad),-np.sin(thetaRad),0.0],
                     [np.sin(thetaRad),np.cos(thetaRad),0.0],
                     [0.0,0.0,1.0]])
    orig_shape = U.shape
    Uf = np.reshape(U,np.size(U))
    Vf = np.reshape(V,np.size(V))
    Wf = np.reshape(W,np.size(W))
    print(Uf.shape)
    vel_rot = np.matmul(Rz,[Uf,Vf,Wf])
    Ur = np.reshape(vel_rot[0,:],orig_shape)
    Vr = np.reshape(vel_rot[1,:],orig_shape)
    Wr = np.reshape(vel_rot[2,:],orig_shape)
    return Ur,Vr,Wr

# Vector functions

def unitVect(p1, p2):
    """
    Get unit vector components parallel to the line join p1 and p1

    Parameters
    ----------
    p1 : numpy.array,float
        vector containing cartesian coordinates of point p1.
    p2 : numpy.array,float
        vector containing cartesian coordinates of point p1.

    Returns
    -------
    uVec : numpy.array, float
        vector containing cartesian coordinates of unit vector along (p1,p1).

    """
    nd = min(len(p1),len(p2))
    uVec = np.array(p2[0:nd])-np.array(p1[0:nd])
    uVec = uVec / np.linalg.norm(uVec)
    return uVec


def circSign2D(v1,v2):
    """
    Calculate the direction of circulation around a pair of edge vectors in 
    the x-y plane.

    Parameters
    ----------
    v1 : numpy.array,float
        components of edge vector v1 in cartesian coordinates.
    v2 : numpy.array,float
        components of edge vector v2 in cartesian coordinates.

    Returns
    -------
    csgn : float
        sign of circulation direction (i.e. +/- 1).

    """
    w = np.cross(v1[0:2],v2[0:2])
    csgn = w / np.linalg.norm(w)
    return csgn

