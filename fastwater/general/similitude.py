# -*- coding: utf-8 -*-
"""
Similitude parameters for the characterisation of fluid flows.
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import numpy as np
# Non-Standard Python Dependencies
# Local Module Dependencies
from fastwater.general.physics import grav
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------
def courantNumber(node, nodes, nodesX, nodesY, velX, velY, timestep):
    iref = np.where(nodes == node)
    ipts = np.where(nodes != node)
    dx = nodesX[ipts]-nodesX[iref]
    dy = nodesY[ipts]-nodesY[iref]
    dr = np.sqrt(np.square(dx)+np.square(dy))
    vel = np.sqrt(np.square(velX)+np.square(velY))
    ux = dx/dr
    uy = dy/dr
    velR = ux*velX+uy*velY
    Cr = velR*timestep/dr
    Cr = np.max(Cr)
    Cr = vel*timestep/np.min(dr)
    return Cr


def froudeNumber(dpth, vel):
    Fr = vel/np.sqrt(grav*dpth)
    return Fr


def ekmanNumber(dpth, omega, kinematicVisc=1.04E-6):
    Ek = kinematicVisc/(omega*np.square(dpth))
    return Ek


def rossbyNumber(dpth, omega):
    Ro = np.sqrt(grav*np.abs(dpth))/np.abs(omega)
    return Ro

