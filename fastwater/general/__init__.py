__path__ = __import__('pkgutil').extend_path(__path__, __name__)
import fastwater.general.timeFuncs
import fastwater.general.fileTools
import fastwater.general.geometry
import fastwater.general.statsTools
import fastwater.general.physics
import fastwater.general.spectral
import fastwater.general.meshTools
import fastwater.general.substrates
import fastwater.general.similitude
