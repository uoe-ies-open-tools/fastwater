#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
# Local Module Dependencies
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------
class tidalTurbine:
    def __init__(self):
        self.model = ''
        self.designer = ''
        self.bottomMounted = True
        self.rotorDiam = None
        self.hubHeight = None
        self.cutInSpeed = None
        self.cutOutSPeed = None
        self.ratedSpeed = None
        self.Cp = None



# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------

def tecD10():
    # Sabella D10
    D10 = tidalTurbine
    D10.model = 'D10'
    D10.designer = 'Sabella'
    D10.bottomMounted = True
    D10.rotorDiam = 10.0
    D10.hubHeight = 11.0
    D10.cutInSpeed = 0.4
    D10.cutOutSpeed = 4.0
    D10.ratedSpeed = 3.1
    D10.Cp = 0.39
    return D10

def tecDeepGenIV():
    # DeepGen IV
    DeepGenIV = tidalTurbine
    DeepGenIV.model = 'DeepGenIV'
    DeepGenIV.designer = 'Alstrom'
    DeepGenIV.bottomMounted = True
    DeepGenIV.rotorDiam = 20.0
    DeepGenIV.hubHeight = 18.0
    DeepGenIV.cutInSpeed = 1.0
    DeepGenIV.cutOutSpeed = 4.0
    DeepGenIV.ratedSpeed = 2.7
    DeepGenIV.Cp = 0.39
    return DeepGenIV


def tecHS1000():
    # HS1000
    HS1000 = tidalTurbine
    HS1000.model = 'HammerFest HS1000'
    HS1000.designer = 'Andritz Hydro'
    HS1000.bottomMounted = True
    HS1000.rotorDiam = 21.0
    HS1000.hubHeight = 15.0
    HS1000.cutInSpeed = 1.0
    HS1000.cutOutSpeed = 4.0
    HS1000.ratedSpeed = 3.15
    HS1000.Cp = 0.33
    return HS1000


def tecAR1500():
    # AR1000
    AR1500 = tidalTurbine
    AR1500.model = 'AR1000'
    AR1500.designer = 'SIMEC-Atlantis'
    AR1500.bottomMounted = True
    AR1500.rotorDiam = 18.0
    AR1500.hubHeight = 14.0
    AR1500.cutInSpeed = 1.0
    AR1500.cutOutSpeed = 5.0
    AR1500.ratedSpeed = 3.00
    AR1500.Cp = 0.39
    return AR1500


def tecAR2000():
    # AR2000
    AR2000 = tidalTurbine
    AR2000.model = 'AR2000'
    AR2000.designer = 'SIMEC-Atlantis'
    AR2000.bottomMounted = True
    AR2000.rotorDiam = 20.0
    AR2000.hubHeight = 15.0
    AR2000.cutInSpeed = 1.0
    AR2000.cutOutSpeed = 4.0
    AR2000.ratedSpeed = 3.05
    AR2000.Cp = 0.36
    return AR2000


def tecOrbitalO2():
    # Orbital O2
    Orbital = tidalTurbine
    Orbital.model = 'Orbital O2'
    Orbital.designer = 'Orbital Marine Power'
    Orbital.bottomMounted = False
    Orbital.rotorDiam = 22.0
    Orbital.hubHeight = 14.0
    Orbital.cutInSpeed = 1.0
    Orbital.cutOutSpeed = 4.5
    Orbital.ratedSpeed = 2.5
    Orbital.Cp = 0.36
    return Orbital


def tecNova100D():
    # Nova Innovation 100_D
    Nova100D = tidalTurbine
    Nova100D.model = '100_D'
    Nova100D.designer = 'Nova Innovation'
    Nova100D.bottomMounted = True
    Nova100D.rotorDiam = 8.5
    Nova100D.hubHeight = 9.0
    Nova100D.cutInSpeed = 0.5
    Nova100D.cutOutSpeed = 6.0
    Nova100D.ratedSpeed = 2.0
    Nova100D.Cp = 0.43
    return Nova100D


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

#def main():
#    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

#if __name__ == "__main__":
#    main()

