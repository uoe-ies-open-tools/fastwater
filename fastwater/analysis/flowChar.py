# -*- coding: utf-8 -*-
"""
Flow Characterisation tools

Created on Mon Aug  1 15:05:12 2022

@author: cold2
"""


# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
import numpy as np
# Non-Standard Python Dependencies
from sklearn.cluster import KMeans
from scipy.signal import convolve
# Local Module Dependencies
from fastwater.general.timeFuncs import dateStr, dateNumFromStr
from fastwater.general.fileTools import getListOfFiles
from fastwater.general.fileTools import loadmat
# Other Dependencies
from fastwater.analysis.dataReaders import netcdfGeneric as ncfg

# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------

def moving_average(x, w):
    return convolve(x, np.ones(w), method='direct') / w


def movmean(v,n):
    ret = np.nancumsum(v, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    mavg = ret[n:] / n
    return mavg


def chord_area(R,h):
    F1 = 0.5*np.pi*R*R
    F2 = R*R*np.arcsin(h/R)
    F3 = h * np.sqrt(R*R - h*h)
    Ac = F1-F2-F3
    return Ac


def pwra_vel(z,v,zmin,zmax,nz=3):
    nsegs = nz-1
    dz = (zmax-zmin)/(nsegs)
    zr = np.arange(nz)*dz
    vz = np.interp(zr,z,v)
    dv = (vz[0:-1]+vz[1:])/2.0
    r = (zmax-zmin)/2.0
    vr = 0.0
    A = 0.0
    for seg in np.arange(nsegs):
        h1 = zr[seg]-r
        h2 = zr[seg+1]-r
        Ac1 = chord_area(r,h1)
        Ac2 = chord_area(r,h2)
        dA = np.abs(Ac2-Ac1)
        vr = vr + dA * np.power(dv[seg],3)
        A = A + dA
    pwra = np.power(np.abs(vr)/A,1.0/3.0)
    return pwra


def rotor_average_vel(z,v,zmin,zmax,nz=3):
    dz = (zmax-zmin)/(nz-1)
    zr = zmin + np.arange(nz)*dz
    vr = np.interp(zr,z,v)
    rotav = np.nanmean(vr)
    return rotav


def tec_power(vel_rot,rdia,Cp=0.33,rho_sw=1025.0):
    A = np.pi*np.square(rdia/2.0)
    Pwr = 0.5*rho_sw*A*np.power(vel_rot,3)*Cp
    return Pwr


def streamwise_flow(ppdata):
    u = ppdata['eastward_vel']
    v = ppdata['northward_vel']
    
    ebbi = ppdata['ebb_indices']
    ebb_angle = ppdata['peak_dir']['dir'][ppdata['peak_id']['ebb']]
    ebb_uv = [np.cos(np.radians(ebb_angle)),np.sin(np.radians(ebb_angle))]
    ebbu = np.tile(ebb_uv,(len(ebbi),1)).transpose()
    
    fldi = ppdata['flood_indices']
    fld_angle = ppdata['peak_dir']['dir'][ppdata['peak_id']['flood']]
    fld_uv = [np.cos(np.radians(fld_angle)),np.sin(np.radians(fld_angle))]
    fldu = np.tile(fld_uv,(len(fldi),1)).transpose()

    vel_strm = np.empty(u.shape)
    vel_strm.fill(np.nan)
    
    print('  Constructing streamwise velocity data...')
    vel_lay = np.vstack((u[ebbi],v[ebbi]))
    vel_strm[ebbi] = np.sum(vel_lay*ebbu,0)
        
    del vel_lay
    vel_lay = np.vstack((u[fldi],v[fldi]))
    vel_strm[fldi] = np.sum(vel_lay*fldu,0)
    
    return vel_strm

def flow_acceleration(data):

    t = data['time']
    u = data['eastward_vel']
    v = data['northward_vel']

    dt = np.median(np.diff(t))*24.0*60.0*60.0

    dudt = np.gradient(u)/dt
    dvdt = np.gradient(v)/dt
    
    magn = np.sqrt(np.power(dudt,2) + np.power(dvdt,2))
    dirn = np.rad2deg(np.arctan2(dvdt,dudt))

    flw_acc = {}
    flw_acc['time'] = t
    flw_acc['dudt'] = dudt
    flw_acc['dvdt'] = dvdt
    flw_acc['magn'] = magn
    flw_acc['dirn'] = dirn
    
    return flw_acc


def get_flood_ebb_indices(thta,pkDir,fld_id):
    mean_dir = pkDir['mean_dir']
    
    fld_peak_dir = pkDir['dir'][fld_id]
    
    # Flood and Ebb angle ranges
    if (fld_peak_dir - mean_dir) < 0:
        fldangles = [mean_dir-180.0,mean_dir]
        if fldangles[0] < -180.0:
            fldangles = [[-180.0,mean_dir],[fldangles[0]+360.0, 180.0]]
        ebbangles = [mean_dir,mean_dir+180.0]
        if ebbangles[1] > 180.0:
            ebbangles = [[mean_dir,180.0],[-180.0,ebbangles[1]-360.0]]
    else:
        fldangles = [mean_dir,mean_dir+180.0]
        if fldangles[1] > 180.0:
            fldangles = [[mean_dir,180.0],[-180.0,fldangles[1]-360.0]]
        ebbangles = [mean_dir-180.0,mean_dir]
        if ebbangles[0] < -180.0:
            ebbangles = [[-180.0,mean_dir],[ebbangles[0]+360.0, 180.0]]
    
    # Get indices
    if len(np.asarray(fldangles).shape) == 1:
        fldind = np.where((thta > fldangles[0]) & (thta < fldangles[1]))[0]
    else:
        fldind = np.where(((thta > fldangles[0][0]) & (thta < fldangles[0][1])) | ((thta > fldangles[1][0]) & (thta < fldangles[1][1])))[0]
    
    flood_indices = fldind
    
    if len(np.asarray(ebbangles).shape) == 1:
        ebbind = np.where((thta > ebbangles[0]) & (thta < ebbangles[1]))[0]
    else:
        ebbind = np.where(((thta > ebbangles[0][0]) & (thta < ebbangles[0][1])) | ((thta > ebbangles[1][0]) & (thta < ebbangles[1][1])))[0]
    
    ebb_indices = ebbind

    return [flood_indices, ebb_indices]


def id_tidal_flow_peaks(thta,elv,pkDir):
    peak_id = {}
    gdindx = np.where(~np.isnan(elv))[0]
    fidx = np.where(elv[gdindx] > 0.25*np.nanmax(elv))[0]
    if fidx.size == 0:
        peak_id['flood'] = 0
        peak_id['ebb'] = 1
    else:
        fldang = np.nanmedian(thta[fidx])
        delang = np.abs(pkDir['dir']-fldang)
        peak_id['flood'] = np.where(delang == np.min(delang))[0][0]
        if peak_id['flood'] == 0:
            peak_id['ebb'] = 1
        else:
            peak_id['ebb'] = 0
    return peak_id


def tidal_flow_peak_directions(thta,magn):

    gdindx = np.where(~np.isnan(magn))[0]
    kmeans = KMeans(n_clusters=2)
    X = np.vstack((thta[gdindx],magn[gdindx])).transpose()
    kmeans.fit(X)
    km = kmeans.labels_
    
    pk1 = np.where(km == 0)[0]
    spdrng = np.where(magn[gdindx[pk1]] > 0.5*np.nanmax(magn))[0]
    if spdrng.size == 0:
        spdrng = np.where(magn[gdindx[pk1]] > 0.25*np.nanmax(magn))[0]

    if spdrng.size == 0:
        spdrng = np.where(magn[gdindx[pk1]] >= 0.0)[0]

    pk1dir = np.median(thta[gdindx[pk1[spdrng]]])
    if pk1dir > 0:
        pk1bearing = 90.0 - pk1dir
    else:
        pk1bearing = 90.0 - (360.0 + pk1dir)

    if pk1bearing < 0:
        pk1bearing = 360.0 + pk1bearing

    pk2 = np.where(km == 1)[0]
    spdrng = np.where(magn[gdindx[pk2]] > 0.5*np.nanmax(magn))[0]
    if spdrng.size == 0:
        spdrng = np.where(magn[gdindx[pk2]] > 0.25*np.nanmax(magn))[0]

    if spdrng.size == 0:
        spdrng = np.where(magn[gdindx[pk2]] >= 0.0)[0]

    pk2dir = np.median(thta[gdindx[pk2[spdrng]]])
    if pk2dir > 0:
        pk2bearing = 90.0 - pk2dir
    else:
        pk2bearing = 90.0 - (360.0 + pk2dir)

    if pk2bearing < 0:
        pk2bearing = 360.0 + pk2bearing


    deldir = np.abs(pk2dir-pk1dir)
    if deldir > 180.0:
        deldir = 360.0-deldir
    meandir = (pk1dir+pk2dir)/2.0

    pkDir = {}
    pkDir['dir'] = [pk1dir,pk2dir]
    pkDir['bearing'] = [pk1bearing,pk2bearing]
    pkDir['del_dir'] = deldir
    pkDir['mean_dir'] = meandir
    
    return pkDir


def preprocess_ts(tseries,tec=None,ndays=None,tstart=None):
    
    # Convert velocity to magnitude and direction
    t = tseries['Timestamp'].copy()
    epoch = tseries['Epoch']
    epoch_off = dateNumFromStr(epoch.replace('/','-'))
    if ndays is None:
        # third parameter does not exist, so default it to something
        ndays = np.floor(t[-1]-t[0])+1
        start_date = dateStr(np.floor(t[0]+epoch_off))
        end_date = dateStr(t[-1]+epoch_off)
        npts = len(t)
        indx1 = 0
        indx2 = npts
    else:
        dt = np.median(np.diff(t))
        ndaysoffset = np.int64(np.ceil(ndays/dt))
        if tstart is None:
            start_date = dateStr(np.floor(t[0]+epoch_off))
            indx1 = 1
            end_date = dateStr(np.floor(t[ndaysoffset]+epoch_off))
        else:
            start_date = dateStr(tstart)
            indx_t = np.where(t >= tstart-epoch_off)[0]
            indx1 = indx_t[0]
            end_date = dateStr(np.floor(t[indx1+ndaysoffset]+epoch_off))

        npts = ndaysoffset
        indx2 = indx1 + ndaysoffset

    deploystr = '( Data covers '+str(ndays)+' days from '+start_date+' )'

    if 'DEPTH' in tseries.keys():
        dth = tseries['DEPTH'][indx1:indx2].copy()
    else:
        dth = tseries['Pressure'][indx1:indx2].copy()
        if np.sum(np.diff(dth)) == 0:
            dth[:] = np.nan

    msl = np.nanmedian(dth)
    elv = dth - msl
    baddth = np.where(np.abs(elv) > 1.5*np.nanmax(elv))[0]
    dth[baddth] = np.nan
    msl = np.nanmedian(dth)
    elv = dth - msl
    
    u = tseries['EAST'][indx1:indx2]
    v = tseries['NORTH'][indx1:indx2]
    w = tseries['UP'][indx1:indx2]
    
    u[baddth] = np.nan
    v[baddth] = np.nan
    w[baddth] = np.nan

    magn = np.sqrt(np.power(u[0:npts],2) + np.power(v[0:npts],2))
    thta = np.rad2deg(np.arctan2(v[0:npts],u[0:npts]))

    res_magn = np.sqrt(np.power(np.nanmean(u[0:npts]),2) + np.power(np.nanmean(v[0:npts]),2))
    res_thta = np.rad2deg(np.arctan2(np.nanmean(v[0:npts]),np.nanmean(u[0:npts])))
    if res_thta < -180.0:
        res_thta = res_thta + 360.0
    elif res_thta > 180.0:
        res_thta = res_thta - 360.0

    # Find tidal flow direction peaks
    pkdir = tidal_flow_peak_directions(thta,magn)

    # Identify flood peak
    peak_id = id_tidal_flow_peaks(thta,elv,pkdir)

    # Get Flood and Ebb Indices
    [fldindices, ebbindices] = get_flood_ebb_indices(thta,pkdir,peak_id['flood'])

    # Calculate percentage of time in flow speed ranges (below cut-in, operating, above rated)
    if tec is not None:
        fldpctage = [100.0*len(np.where(magn[fldindices] < tec.cutInSpeed)[0])/len(fldindices), \
                     100.0*len(np.where((magn[fldindices] >= tec.cutInSpeed) & (magn[fldindices] <= tec.ratedSpeed))[0])/len(fldindices), \
                     100.0*len(np.where(magn[fldindices] > tec.ratedSpeed)[0])/len(fldindices)]
    
        ebbpctage = [100.0*len(np.where(magn[ebbindices] < tec.cutInSpeed)[0])/len(ebbindices), \
                     100.0*len(np.where((magn[ebbindices] >= tec.cutInSpeed) & (magn[ebbindices] <= tec.ratedSpeed))[0])/len(ebbindices), \
                     100.0*len(np.where(magn[ebbindices] > tec.ratedSpeed)[0])/len(ebbindices)]
    else:
        fldpctage = [np.nan, np.nan, np.nan]
        ebbpctage = [np.nan, np.nan, np.nan]

    ngood = len(fldindices) + len(ebbindices)
    fldpc = 100.0*len(fldindices)/ngood
    ebbpc = 100.0*len(ebbindices)/ngood

    # Calculate percentage of time flow in direction bins around peak
    flddir = pkdir['dir'][peak_id['flood']]
    ebbdir = pkdir['dir'][peak_id['ebb']]

    dfldang = thta[fldindices]-flddir
    dfldang[dfldang < -180.0] = dfldang[dfldang < -180.0] + 360.0
    dfldang[dfldang > 180.0] = dfldang[dfldang > 180.0] - 360.0

    debbang = thta[ebbindices]-ebbdir
    debbang[debbang < -180.0] = debbang[debbang < -180.0] + 360.0
    debbang[debbang > 180.0] = debbang[debbang > 180.0] - 360.0

    # Collect Information
    data = {}
    data['dataset'] = tseries['DataSet']
    data['datasrc'] = tseries['DataSrc']
    data['deploystr'] = deploystr
    data['dataloc'] = tseries['DataLoc']
    data['crs'] = tseries['CRS']
    data['hab'] = tseries['hab']
    data['zloc'] = tseries['zloc']
    data['time'] = t[indx1:indx2]
    data['epoch'] = epoch
    data['number_of_days'] = ndays
    data['start_date'] = start_date
    data['end_date'] = end_date
    data['eastward_vel'] = u
    data['northward_vel'] = v
    data['upward_vel'] = w
    data['thta'] = thta
    data['magn'] = magn
    data['res_thta'] = res_thta
    data['res_magn'] = res_magn
    data['peak_dir'] = pkdir
    data['peak_id'] = peak_id
    data['flood_indices'] = fldindices
    data['ebb_indices'] = ebbindices
    data['del_flood_dir'] = dfldang
    data['del_ebb_dir'] = debbang
    data['flood_total_percent'] = fldpc
    data['flood_percent'] = fldpctage
    data['ebb_total_percent'] = ebbpc
    data['ebb_percent'] = ebbpctage
    data['depth'] = dth
    data['msl'] = msl
    data['surface_elev'] = elv
    if 'DataFile' in tseries.keys():
        data['datafile'] = tseries['DataFile'].copy()
        data['data_index_1'] = tseries['DataIndex1']
        data['data_index_2'] = tseries['DataIndex2']

    # Calculate flow acceleration
    flw_acc = flow_acceleration(data)
    
    # Add flow acceleration
    data['flw_acc'] = flw_acc
    
    return data


# Data extraction and conversion functions
#=========================================

def extract_model_rotav(datapath,srchstr,dataset,datasrc,ppdata,rdia,zhub,hab=True,pwra=True,Cp=0.33):

    # Set rotor z range
    if hab:
        zmin = zhub - rdia/2.0
        zmax = zhub + rdia/2.0
    else:
        zmin = zhub + rdia/2.0
        zmax = zhub - rdia/2.0
    
    ebbi = ppdata['ebb_indices']
    ebb_angle = ppdata['peak_dir']['dir'][ppdata['peak_id']['ebb']]
    ebb_uv = [np.cos(np.radians(ebb_angle)),np.sin(np.radians(ebb_angle))]
    ebbu = np.tile(ebb_uv,(len(ebbi),1)).transpose()
    
    fldi = ppdata['flood_indices']
    fld_angle = ppdata['peak_dir']['dir'][ppdata['peak_id']['flood']]
    fld_uv = [np.cos(np.radians(fld_angle)),np.sin(np.radians(fld_angle))]
    fldu = np.tile(fld_uv,(len(fldi),1)).transpose()

    # Get list of files to process
    files = getListOfFiles(datapath,srchstr+'*.mat')
    
    # Initialise output data structure
    t_series = {}
    t_series['DataSet'] = dataset
    t_series['DataSrc'] = datasrc
    ts = []
    dpth = []
    vel_rot = []
    u = []
    v = []
    z = []
    
    for indx, f in enumerate(files):
        
        print(str(indx)+'     '+f)
        
        vel = loadmat(datapath+'/'+f)
        
        if indx == 0:
            dataloc = vel['dataLoc']
            crs = vel['locCRS']
            epoch = vel['epoch']
        
            ts = vel['times'][1:].copy()
            dpth = vel['depth'][1:].copy()
                
            u = vel['vel_east'][1:,:].copy()
            v = vel['vel_north'][1:,:].copy()
            z = vel['z'][1:,:].copy()
            
        else:
            ts = np.hstack((ts,vel['times'][1:].copy()))
            dpth = np.hstack((dpth,vel['depth'][1:].copy()))
                
            u = np.vstack((u,vel['vel_east'][1:,:].copy()))
            v = np.vstack((v,vel['vel_north'][1:,:].copy()))
            z = np.vstack((z,vel['z'][1:,:].copy()))
        
    nrecs = u.shape[0]
    nlays = u.shape[1]
    
    vel_strm = np.empty(u.shape)
    vel_strm.fill(np.nan)
    
    print('  Constructing streamwise velocity data...')
    for ilay in np.arange(nlays):
        
        vel_lay = np.vstack((u[ebbi,ilay],v[ebbi,ilay]))
        vel_strm[ebbi,ilay] = np.sum(vel_lay*ebbu,0)
        
        del vel_lay
        vel_lay = np.vstack((u[fldi,ilay],v[fldi,ilay]))
        vel_strm[fldi,ilay] = np.sum(vel_lay*fldu,0)
    
    print('  Calculating rotor average velocity..')
    vel_rot = np.empty((nrecs,))
    for irec in np.arange(nrecs):
        ht = z[irec,:]-z[irec,0]
        if not hab:
            zminp = ht[-1]-zmin
            zmaxp = ht[-1]-zmax
        else:
            zminp = zmin
            zmaxp = zmax
                    
        if pwra:
            vel_rot[irec] = pwra_vel(ht,vel_strm[irec,:],zminp,zmaxp,11)
        else:
            vel_rot[irec] = rotor_average_vel(ht,vel_strm[irec,:],zminp,zmaxp,11)
    
    tidal_pwr = tec_power(vel_rot,rdia,Cp=Cp)
    
    print('  Collating results into dictionary structure...')
    
    t_series['dataloc'] = dataloc
    t_series['crs'] = crs
    t_series['hab'] = hab
    t_series['zloc'] = zhub
    if hab:
        t_series['mooring'] = 'bottom'
    else:
        t_series['mooring'] = 'floating'
    t_series['rdia'] = rdia
    t_series['ebb_dir'] = ebb_angle
    t_series['ebb_indices'] = ebbi
    t_series['flood_dir'] = fld_angle
    t_series['flood_indices'] = fldi
    t_series['timestamp'] = ts
    t_series['epoch'] = epoch
    t_series['depth'] = dpth
    t_series['vel_rot'] = vel_rot
    t_series['tec_pwr'] = tidal_pwr

    return t_series


def extract_model_depthavg(datapath,srchstr,dataset,datasrc,zhub,hab=True):
    # Get list of files to process
    files = getListOfFiles(datapath,srchstr+'*.mat')
    
    # Initialise output data structure
    t_series = {}
    t_series['DataSet'] = dataset
    t_series['DataSrc'] = datasrc
    ts = []
    dpth = []
    udav = []
    vdav = []
    
    for indx, f in enumerate(files):
        
        print(str(indx)+'     '+f)
        
        vel = loadmat(datapath+'/'+f)
        
        if indx == 0:
            dataloc = vel['dataLoc']
            crs = vel['locCRS']
            epoch = vel['epoch']
        
        ts = np.hstack((np.asarray(ts),vel['times'][1:]))
        dpth = np.hstack((np.asarray(dpth),vel['depth'][1:]))
            
        u = vel['DepAvg_U'][1:]
        v = vel['DepAvg_V'][1:]
        
        udav = np.hstack((np.asarray(udav),u))
        vdav = np.hstack((np.asarray(vdav),v))
        
        del vel, u, v
        
    t_series['DataLoc'] = dataloc
    t_series['CRS'] = crs
    t_series['hab'] = hab
    t_series['zloc'] = zhub
    if hab:
        t_series['mooring'] = 'bottom'
    else:
        t_series['mooring'] = 'floating'
    t_series['Timestamp'] = ts
    t_series['Epoch'] = epoch
    t_series['DEPTH'] = dpth
    t_series['EAST'] = udav
    t_series['NORTH'] = vdav
    t_series['UP'] = np.zeros(udav.shape)

    return t_series


def extract_model_ts_at_height(datapath,srchstr,dataset,datasrc,zhub,hab=True):
    # Get list of files to process
    files = getListOfFiles(datapath,srchstr+'*.mat')
    
    # Initialise output data structure
    t_series = {}
    t_series['DataSet'] = dataset
    t_series['DataSrc'] = datasrc
    ts = []
    dpth = []
    uhab = []
    vhab = []
    whab = []
    ugrd = []
    vgrd = []
    wgrd = []
    
    for indx, f in enumerate(files):
        
        print(str(indx)+'     '+f)
        
        vel = loadmat(datapath+'/'+f)
        
        if indx == 0:
            dataloc = vel['dataLoc']
            crs = vel['locCRS']
            epoch = vel['epoch']
        
        ts = np.hstack((np.asarray(ts),vel['times'][1:]))
        dpth = np.hstack((np.asarray(dpth),vel['depth'][1:]))
            
        u = vel['vel_east'][1:,:]
        v = vel['vel_north'][1:,:]
        w = vel['vel_up'][1:,:]
        z = vel['z'][1:,:]
        
        nrecs = len(u)
        
        uh = np.empty((nrecs,))
        uh.fill(np.nan)
        vh = np.empty((nrecs,))
        vh.fill(np.nan)
        wh = np.empty((nrecs,))
        wh.fill(np.nan)
        ug = np.empty((nrecs,))
        ug.fill(np.nan)
        vg = np.empty((nrecs,))
        vg.fill(np.nan)
        wg = np.empty((nrecs,))
        wg.fill(np.nan)
        
        for irec in np.arange(nrecs):
            ht = z[irec,:]-z[irec,0]
            if not hab:
                zh = ht[-1]-zhub
            else:
                zh = zhub
            dudz = np.gradient(u[irec,:],ht)
            dvdz = np.gradient(v[irec,:],ht)
            dwdz = np.gradient(w[irec,:],ht)
            
            uh[irec] = np.interp(zh,ht,u[irec,:])
            vh[irec] = np.interp(zh,ht,v[irec,:])
            wh[irec] = np.interp(zh,ht,w[irec,:])

            ug[irec] = np.interp(zh,ht,dudz)
            vg[irec] = np.interp(zh,ht,dvdz)
            wg[irec] = np.interp(zh,ht,dwdz)
            
        uhab = np.hstack((np.asarray(uhab),uh))
        vhab = np.hstack((np.asarray(vhab),vh))
        whab = np.hstack((np.asarray(whab),wh))
        
        ugrd = np.hstack((np.asarray(ugrd),uh))
        vgrd = np.hstack((np.asarray(vgrd),vh))
        wgrd = np.hstack((np.asarray(wgrd),wh))
        
        del vel, u, v, w, z, nrecs
        del ht, uh, vh, wh
        
    t_series['DataLoc'] = dataloc
    t_series['CRS'] = crs
    t_series['hab'] = hab
    t_series['zloc'] = zhub
    if hab:
        t_series['mooring'] = 'bottom'
    else:
        t_series['mooring'] = 'floating'
    t_series['Timestamp'] = ts
    t_series['Epoch'] = epoch
    t_series['DEPTH'] = dpth
    t_series['EAST'] = uhab
    t_series['NORTH'] = vhab
    t_series['UP'] = whab
    t_series['dudz'] = ugrd
    t_series['dvdz'] = vgrd
    t_series['dwdz'] = wgrd

    return t_series


def extract_redapt_motion(datapath,srchstr,dataset,datasrc):
    """
    Extract data from archive ReDAPT ADCP data in netCDF format

    Parameters
    ----------
    datapath : str
        DESCRIPTION.
    srchstr : str
        DESCRIPTION.
    dataset : str
        DESCRIPTION.
    datasrc : str
        DESCRIPTION.

    Returns
    -------
    motion : dict
        DESCRIPTION.

    """
    # Get list of files to process
    files = getListOfFiles(datapath+'/'+dataset,srchstr+'*.nc')
    
    # Initialise output data structure
    motion = {}
    motion['DataSet'] = dataset
    motion['DataSrc'] = datasrc
    time = []
    pres = []
    ptch = []
    roll = []
    head = []
    
    # Loop over files and extract data and time avarage
    for indx, f in enumerate(files):
        
        print(str(indx)+'     '+f)
        
        data = ncfg(datapath+'/'+dataset+'/'+f)
        
        # get timestamps
        #if 'epoch_str' in data.getVarAttrList('times'):
        if 'epoch_str' in data.listVarAttrs('times'):
            epoch = data.getVarAttr('times','epoch_str')
        else:
            epoch = None
        t = data.getVar('times')
        
        if indx == 0:
            bang = data.getVar('beam_angle')
        
        # Get data
        inst_pres = data.getVar('pressure')
        inst_ptch = data.getVar('pitch')
        inst_roll = data.getVar('roll')
        inst_head = data.getVar('heading')
        
        # Apply QC flags
        qc_dp = data.getVar('qc_deployDepth')*1.0
        qc_rt = data.getVar('qc_rocTilts')*1.0
        
        qc_good_p = (qc_dp + qc_rt[:,0])/2.0
        qc_good_p[qc_good_p>1.0] = np.nan
        qc_good_r = (qc_dp + qc_rt[:,2])/2.0
        qc_good_r[qc_good_r>1.0] = np.nan
        qc_good_h = (qc_dp + qc_rt[:,1])/2.0
        qc_good_h[qc_good_h>1.0] = np.nan
        
        inst_ptch = inst_ptch * qc_good_p * qc_good_r * qc_good_h
        inst_roll = inst_roll * qc_good_p * qc_good_r * qc_good_h
        inst_head = inst_head * qc_good_p * qc_good_r * qc_good_h
        
        time = np.hstack((np.asarray(time),t))
        pres = np.hstack((np.asarray(pres),inst_pres))
        ptch = np.hstack((np.asarray(ptch),inst_ptch))
        roll = np.hstack((np.asarray(roll),inst_roll))
        head = np.hstack((np.asarray(head),inst_head))
        
        del data, t, inst_ptch, inst_roll, inst_head
        
       
    motion['Timestamp'] = time
    motion['Epoch'] = epoch
    motion['beam_angle'] = bang
    motion['Pressure'] = pres
    motion['Pitch'] = ptch
    motion['Roll'] = roll
    motion['Heading'] = head
    
    return motion


def extract_redapt_ts_at_height(datapath,srchstr,dataset,datasrc,zhub,hab=True,smth_win=None,sub_win=None):
    """
    Extract timeseries of velocity data at a fixed height from archived
    ReDAPT ADCP data in netCDF format.

    Parameters
    ----------
    datapath : str
        DESCRIPTION.
    srchstr : str
        DESCRIPTION.
    dataset : str
        DESCRIPTION.
    datasrc : str
        DESCRIPTION.
    zhub : float
        DESCRIPTION.
    hab : bool, optional
        DESCRIPTION. The default is True.
    smth_win : int64, optional
        DESCRIPTION. The default is None.
    sub_win : float, optional
        DESCRIPTION. The default is None.

    Returns
    -------
    t_series : dict
        DESCRIPTION.

    """
    swin = np.int64(smth_win)
    
    # Load t_series
    # Get list of files to process
    files = getListOfFiles(datapath+'/'+dataset,srchstr+'*.nc')
    
    # Initialise output data structure
    t_series = {}
    t_series['DataSet'] = dataset
    t_series['DataSrc'] = datasrc
    thab = []
    dhab = []
    uhab = []
    vhab = []
    whab = []
    
    # Loop over files and extract data and time avarage
    for indx, f in enumerate(files):
        
        print(str(indx)+'     '+f)
        
        data = ncfg(datapath+'/'+dataset+'/'+f)
        
        # Construct z vector
        head_hab = data.getVar('head_hab')
        bin_dist = data.getVar('bin_dist')
        bin_size = data.getVar('bin_size')
        num_bins = data.getVar('num_bins')
        easting = data.getVar('easting')
        northing = data.getVar('northing')
        crs = data.getVarAttr('northing','crs')
        
        z = np.arange(num_bins)*bin_size + bin_dist + head_hab
        
        # get timestamps
        if 'epoch_str' in data.listVarAttrs('times'):
            epoch = data.getVarAttr('times','epoch_str')
        else:
            epoch = None
        
        t = data.getVar('times')
        #if epoch is not None:
        #    t = t + dateNumFromStr(epoch)
        
        dt = np.round(np.nanmedian(np.diff(t))*24.0*60.0*60.0,2)
        
        p = data.getVar('pressure')
        u = data.getVar('vel_geo_east')
        v = data.getVar('vel_geo_north')
        w = data.getVar('vel_geo_vert')
       
        # Apply QC flags
        qc_pr = data.getVar('qc_deployDepth')*1.0
        qc_pr[qc_pr > 1.0] = np.nan
        qc_dp = np.tile(qc_pr,[46,1]).transpose()
        qc_dp[qc_dp > 1.0] = np.nan
        qc_e = data.getVar('qc_rdiFlag_east')*1.0
        qc_e[qc_e > 1.0] = np.nan
        qc_n = data.getVar('qc_rdiFlag_north')*1.0
        qc_n[qc_n > 1.0] = np.nan
        qc_v = data.getVar('qc_rdiFlag_vert')*1.0
        qc_v[qc_v > 1.0] = np.nan
        qc_vl = data.getVar('qc_velocityLimits')*1.0
        qc_vl[qc_vl > 1.0] = np.nan
        qc_rt = data.getVar('qc_rocTilts')*1.0
        qc_ptch = np.tile(qc_rt[:,0],[46,1]).transpose()
        qc_roll = np.tile(qc_rt[:,2],[46,1]).transpose()
                
        p = p * qc_pr
        u = u * qc_e * qc_dp * qc_vl * qc_ptch * qc_roll
        v = v * qc_n * qc_dp * qc_vl * qc_ptch * qc_roll
        w = w * qc_v * qc_dp * qc_vl * qc_ptch * qc_roll
        
        # Interploate onto hab
        nrecs = len(u)
        
        uh = np.empty((nrecs,))
        uh.fill(np.nan)
        vh = np.empty((nrecs,))
        vh.fill(np.nan)
        wh = np.empty((nrecs,))
        wh.fill(np.nan)
        
        for irec in np.arange(nrecs):
            if not hab:
                zh = p[irec]-zhub
            else:
                zh = zhub
            if ~np.isnan(zh):
                uh[irec] = np.interp(zh,z,u[irec,:])
                vh[irec] = np.interp(zh,z,v[irec,:])
                wh[irec] = np.interp(zh,z,w[irec,:])
            else:
                uh[irec] = np.nan
                vh[irec] = np.nan
                wh[irec] = np.nan
                
            
        # moving average smoother
        if smth_win is not None:
            #dhs = movmean(p,swin)
            #uhs = movmean(uh,swin)
            #vhs = movmean(vh,swin)
            #whs = movmean(wh,swin)
            dhs = moving_average(p,swin)[swin//2-1:-swin//2]
            uhs = moving_average(uh,swin)[swin//2-1:-swin//2]
            vhs = moving_average(vh,swin)[swin//2-1:-swin//2]
            whs = moving_average(wh,swin)[swin//2-1:-swin//2]
        else:
            dhs = p
            uhs = uh
            vhs = vh
            whs = wh
        
        # sub-sample to required time window
        if sub_win is not None:
            n_samp = np.int64(np.floor(sub_win/dt))
            thss = t[np.arange(0,nrecs,n_samp)]
            dhss = dhs[np.arange(0,nrecs,n_samp)]
            uhss = uhs[np.arange(0,nrecs,n_samp)]
            vhss = vhs[np.arange(0,nrecs,n_samp)]
            whss = whs[np.arange(0,nrecs,n_samp)]
        else:
            thss = t
            dhss = dhs
            uhss = uhs
            vhss = vhs
            whss = whs
        
        thab = np.hstack((np.asarray(thab),thss))
        dhab = np.hstack((np.asarray(dhab),dhss))
        uhab = np.hstack((np.asarray(uhab),uhss))
        vhab = np.hstack((np.asarray(vhab),vhss))
        whab = np.hstack((np.asarray(whab),whss))
        
        del data, t, p, u, v, w, z, nrecs
        del uh, vh, wh
        del dhs, uhs, vhs, whs
        del thss, dhss, uhss, vhss, whss
        
       
    t_series['DataLoc'] = [easting,northing]
    t_series['CRS'] = crs
    t_series['hab'] = hab
    t_series['zloc'] = zhub
    if hab:
        t_series['mooring'] = 'bottom'
    else:
        t_series['mooring'] = 'floating'
    t_series['Timestamp'] = thab
    t_series['Epoch'] = epoch
    t_series['DEPTH'] = dhab
    t_series['EAST'] = uhab
    t_series['NORTH'] = vhab
    t_series['UP'] = whab
    
    return t_series


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

