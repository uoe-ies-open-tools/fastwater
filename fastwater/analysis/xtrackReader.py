#-------------------------------------------------------------------------
# Data Loader for XTRACK *.nc Files
#
# Version: 0.0
# Author: C.P. Old
#
# History:
# 28 January 2020 - Initial code creation and testing
#
#-------------------------------------------------------------------------

import numpy as np
from netCDF4 import Dataset
import utm

#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

def isInDomain(east,north,domain):
    indx, = np.where((east>domain[0])&(east<domain[1])&(north>domain[2])&(north<domain[3]))
    return indx

def latlon2utm(lat,lon,zone=30,region='N'):
    pos = utm.from_latlon(lat[:],lon[:],zone,region)
    east = pos[0]
    north = pos[1]
    return [east,north]

def readConstitName(constit):
    indx = np.where(constit.mask==False)
    name = str(np.asarray(constit)[indx],"UTF-8")
    return name

def extractXTrackValues(xtvars,index):
    harm = []
    for h in np.arange(xtvars['ncnst']):
        constit = xtvars['constit'][h]
        name = readConstitName(constit)
        harm = np.append(harm,name)
    ampl = xtvars['amp'].data[index]
    phase = xtvars['pha'].data[index]
    mserr = xtvars['mse'].data[index]
    return harm,ampl,phase,mserr

def loadVars(ncfile):
    nc = Dataset(ncfile,'r')
    dict = {}
    # dimensions
    dict['ncnst'] = nc.dimensions['constituent'].size
    dict['nmlen'] = nc.dimensions['namelength'].size
    dict['nrecs'] = nc.dimensions['records'].size
    # variables
    dict['constit'] = nc.variables['constituentname'][:]
    dict['lat'] = nc.variables['lat'][:]
    dict['lon'] = nc.variables['lon'][:]
    dict['amp'] = nc.variables['amplitude'][:]
    dict['pha'] = nc.variables['phase_lag'][:]
    dict['mse'] = nc.variables['mean_square_error'][:]
    dict['bce'] = nc.variables['bg_contamination_error'][:]
    dict['smp'] = nc.variables['sample'][:]
    nc.close()
    return dict

def getPointsInDomain(xtvars,domain):
    [east,north] = latlon2utm(xtvars['lat'],xtvars['lon'])
    indx = isInDomain(east,north,domain)
    if np.asarray(indx).size > 0:
        points = np.asarray([east[indx],north[indx]])
    else:
        points = np.asarray([])
    return points,indx

#--------------------------------------------------------------------------
