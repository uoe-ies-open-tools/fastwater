__path__ = __import__('pkgutil').extend_path(__path__, __name__)
import fastwater.analysis.graphics
import fastwater.analysis.dataReaders
import fastwater.analysis.tidal
import fastwater.analysis.flowChar
import fastwater.analysis.validation
import fastwater.analysis.xtrackReader
