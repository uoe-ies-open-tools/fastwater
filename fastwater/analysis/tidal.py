# -*- coding: utf-8 -*-
"""
Harmonic reduction of tidally forced variables

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import datetime
# Non-Standard Python Dependencies
import numpy as np
import scipy.io as sio
from pytides.tide import Tide
from pytides import constituent as tcons
# Local Module Dependencies
from fastwater.general.fileTools import generateGlobalAttributes
from fastwater.general.spectral import nextpow2, signal_from_fft, ssas_fft, fft
from fastwater.general.timeFuncs import dateStr, dateNumFromStr, datetimeFromNum
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

def datenum(dt):
    dnum = []
    for d in dt:
        dn = d.toordinal()+(d-datetime.datetime.fromordinal(d.toordinal())).total_seconds()/(24*60*60)
        dnum.append(dn)
    return dnum


def dnum2dtime(dnum):
    dtime = []
    for i in np.arange(len(dnum)):
        #dtime.append(datetime.datetime.fromordinal(int(dnum[i])) + datetime.timedelta(days=(dnum[i]%1)[0]))
        dtime.append(datetime.datetime.fromordinal(int(dnum[i])) + datetime.timedelta(days=(dnum[i]%1)))
    return dtime


def getHarmonicError(tide,lsq):
    a = tide.model['amplitude'].copy()
    a = a[1:]
    n = len(a)
    pcov = lsq[1].copy()
    pcov = pcov[:n,:n]
    #res = lsq[2]['fvec']
    #print(np.min(res),np.max(res),np.mean(res),np.mean(np.abs(res)),np.mean((res)**2))
    #s_sq = (np.abs(res)).mean()
    #pcov = pcov * s_sq
    mserror = np.abs(np.diagonal(pcov))
    return mserror


def harmonicReduction(param,dt,tidalCons=tcons.noaa,fullOutput=False):
    tide, lsq = Tide.decompose(param,dt,constituents=tidalCons,full_output=True)
    if fullOutput:
        mserror = getHarmonicError(tide,lsq)
        return tide, mserror
    else:
        return tide


def getConstituentSubset(indices):
    subcon = [tcons.noaa[i] for i in indices]
    return subcon


def extractModelValues(model):
    harm = []
    ampl = []
    phase = []
    for h in np.arange(len(model)):
        harm = np.append(harm,model[h][0].name)
        ampl = np.append(ampl,model[h][1])
        phase = np.append(phase,model[h][2])
    return harm,ampl,phase


def saveTide(param,paramStr,dt,tide,outFile,descriptStr=''):
    dnum = datenum(dt)
    glbAttr = generateGlobalAttributes(descriptStr)
    dict = {}
    dict['globalAttributes'] = glbAttr
    dict['paramName'] = paramStr
    dict['times'] = dnum
    dict['param'] = param
    dict['recon'] = tide.at(dt)
    sio.savemat(outFile,dict)
    return

def signalDecomposition(dnum, val, tidalCons=tcons.noaa, cutoff_period:float=25.0):
    #-----------------------------------------------------------------------------
    # Apply tidal harmonic reduction
    #-----------------------------------------------------------------------------
    dt = []
    for t in dnum:
        dt.append(datetimeFromNum(t))
    tide = harmonicReduction(val,dt)
    pred = tide.at(dt)

    #-----------------------------------------------------------------------------
    # Spectral Decomposition Analysis 
    #-----------------------------------------------------------------------------
    L = len(dnum)
    t_days = dnum-np.float64(np.floor(dnum[0]))
    T = np.floor(np.median((np.diff(dnum))*24.0*60.0*60.0)*1000.0)/1000.0
    Fs = 1.0/T
    pw2 = nextpow2(L)
    n = np.power(2.0,pw2)
    f = Fs*np.arange(n)/n
    ts = np.arange(L)*T
    tf = 1.0/f
    
    h_res = np.mean(val)
    h = val-h_res
    
    h_tide = pred
    h_dyn = h-h_tide
    
    X = np.zeros([np.int64(n),])
    X[0:L] = h_dyn
    spec = fft(X)
    lfindx = np.where(tf/(60.0*60.0)>cutoff_period)[0]                # Low freq indices
    h_dyn_lf = signal_from_fft(ts,f,lfindx[1:],spec)*(Fs/(2.0*np.pi)) # Low freq signal
    h_dyn_hf = h_dyn+h_dyn_lf                                         # High freq signal

    datadict = {}
    datadict['dnum'] = dnum
    datadict['t_days'] = t_days
    datadict['dtime'] = dt
    datadict['val'] = val
    datadict['tidal'] = pred
    datadict['nontidal'] = h_dyn
    datadict['nontidal_lowfreq'] = h_dyn_lf
    datadict['nontidal_highfreq'] = h_dyn_hf
    datadict['model'] = tide.model
    
    return datadict
#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

