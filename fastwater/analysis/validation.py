# -*- coding: utf-8 -*-
"""
Tools for applying validation analysis to pre-preocess extracts of model and 
instrument data.

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from fastwater.general.timeFuncs import dateNumFromStr
from fastwater.general.fileTools import loadmat
from fastwater.general.statsTools import stats_metrics, stats_metrics_circular
from fastwater.general.statsTools import display_metrics
from fastwater.analysis.graphics import flow_class_diagram
from fastwater.analysis.graphics import plot_validation_stats
from fastwater.analysis.graphics import speed_bin_histograms
from fastwater.analysis.graphics import directional_spread_plot
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------
def validateFlowParameter(modFile,obsFile,paramList):
    """
    Validation of model velocity data against in situ ADCP data

    Parameters
    ----------
    modFile : str
        Full path to model data file.
    obsFile : str
        Full path to in situ measurements data file.
    paramList : list,str
        List of parameters to be processed.

    Returns
    -------
    metrics : dict
        Dictionary of validation metrics for all, flood and ebb tide states.
    figHnds : dict
        Dictionary of figure handles for all graphics generated.

    """
    # Initialise output
    metrics = {}
    figHnds = {}
    # Load pre-processed model data
    data_mod = loadmat(modFile)
    # Generate flow characterization plot
    fh_mod = flow_class_diagram(data_mod)
    fh_mod_spdh = speed_bin_histograms(data_mod,binwidth=0.1,maxfrac=0.08)
    fh_mod_dspr = directional_spread_plot(data_mod)
    #Load pre-processed observation data
    data_obs = loadmat(obsFile)
    # Generate flow characterization plot
    fh_obs = flow_class_diagram(data_obs)
    fh_obs_spdh = speed_bin_histograms(data_obs,binwidth=0.1,maxfrac=0.08)
    fh_obs_dspr = directional_spread_plot(data_obs)
    
    figHnds['mod_flowchar'] = fh_mod
    figHnds['mod_speedhist'] = fh_mod_spdh
    figHnds['mod_dirnhist'] = fh_mod_dspr
    figHnds['obs_flowchar'] = fh_obs
    figHnds['obs_speedhist'] = fh_obs_spdh
    figHnds['obs_dirnhist'] = fh_obs_dspr
    
    # Extract metadata for plotting and metric calculations
    metadata = {}
    metadata['Obs_Src'] = data_obs['datasrc']
    metadata['Obs_Deploy'] = data_obs['dataset']
    metadata['Mod_Src'] = data_mod['datasrc']
    metadata['DataLoc'] = data_obs['dataloc']
    metadata['CRS'] = 'WGS84 '+data_obs['crs']+' ('+data_mod['crs']+')'
    metadata['hab'] = data_mod['hab']
    metadata['zloc'] = data_mod['zloc']
    metadata['StartDate'] = data_mod['start_date']
    metadata['NumDays'] = data_mod['number_of_days']
    
    # Loop over parameters in list
    for paramStr in paramList:

        # Load parameter time series data
        if paramStr in ['magnitude','magn']:
            metrics[paramStr] = {}
            figHnds[paramStr] = {}
            fieldStr = 'magn'
            metricStr = 'magnitude'
            displayStr = 'Velocity Magnitude'
            unitStr = ' ms'+r'$^{-1}$'
            min_val = 0.0
            max_val = 4.5
        elif paramStr in ['direction','dirn','thta']:
            metrics[paramStr] = {}
            figHnds[paramStr] = {}
            fieldStr = 'thta'
            metricStr = 'direction'
            displayStr = 'Velocity Direction'
            unitStr = r'$^{\circ}$'
            min_val = -180.0
            max_val = 180.0
            
            
        if fieldStr in data_mod.keys():
            # Generate full parameter metrics
            #--------------------------------------
            # Load parameter timeseries data
            t_mod = data_mod['time'].copy()+dateNumFromStr(data_mod['epoch'].replace('/','-'))
            v_mod = data_mod[fieldStr].copy()
            
            t_obs = data_obs['time'].copy()+dateNumFromStr(data_obs['epoch'].replace('/','-'))
            v_obs = data_obs[fieldStr].copy()
            
            # Interpolate obs onto model timestamps
            obs = np.interp(t_mod,t_obs,v_obs)
            
            # Exclude missing data
            good = np.where(~np.isnan(obs))[0]
            mod = v_mod[good]
            obs = obs[good]
    
            # Calculate validation statistics
            metrics_all = stats_metrics(mod,obs,metricStr)
            
            # Show results
            display_metrics(metrics_all,metricStr)
            
            metadata['tideState'] = 'All phases'
            fig_metrics_all = plot_validation_stats(obs,mod,metrics_all,displayStr,unitStr,min_val,max_val,metadata)
            
            del mod, obs, good
            
            # Generate flood tide parameter metrics
            #--------------------------------------
            # Select flood only data
            ebbind = data_mod['ebb_indices']
            t_mod = data_mod['time'].copy().copy()+dateNumFromStr(data_mod['epoch'].replace('/','-'))
            t_mod[ebbind] = np.nan
            v_mod = data_mod[fieldStr].copy()
            v_mod[ebbind] = np.nan
            del ebbind
            
            ebbind = data_obs['ebb_indices']
            t_obs = data_obs['time'].copy()+dateNumFromStr(data_obs['epoch'].replace('/','-'))
            t_obs[ebbind] = np.nan
            v_obs = data_obs[fieldStr].copy()
            v_obs[ebbind] = np.nan
            del ebbind
            
            # Interpolate obs onto model timestamps
            obs = np.interp(t_mod,t_obs,v_obs)
            
            # Exclude missing data
            good = np.where((~np.isnan(v_mod)) & (~np.isnan(obs)))[0]
            mod = v_mod[good]
            obs = obs[good]
            
            # Calculate validation statistics
            metrics_fld = stats_metrics(mod,obs,metricStr)
            
            # Show results
            display_metrics(metrics_fld,metricStr)
            
            metadata['tideState'] = 'Flood only'
            fig_metrics_fld = plot_validation_stats(obs,mod,metrics_fld,displayStr,unitStr,min_val,max_val,metadata)
            
            del mod, obs, good
    
            # Generate ebb tide parameter metrics
            #--------------------------------------
            # Select ebb only data
            fldind = data_mod['flood_indices']
            t_mod = data_mod['time'].copy()+dateNumFromStr(data_mod['epoch'].replace('/','-'))
            t_mod[fldind] = np.nan
            v_mod = data_mod[fieldStr].copy()
            v_mod[fldind] = np.nan
            del fldind
            
            fldind = data_obs['flood_indices']
            t_obs = data_obs['time'].copy()+dateNumFromStr(data_obs['epoch'].replace('/','-'))
            t_obs[fldind] = np.nan
            v_obs = data_obs[fieldStr].copy()
            v_obs[fldind] = np.nan
            del fldind
            
            # Interpolate obs onto model timestamps
            obs = np.interp(t_mod,t_obs,v_obs)
            
            # Exclude missing data
            good = np.where((~np.isnan(v_mod)) & (~np.isnan(obs)))[0]
            mod = v_mod[good]
            obs = obs[good]
            
            # Calculate validation statistics
            metrics_ebb = stats_metrics(mod,obs,metricStr)
            
            # Show results
            display_metrics(metrics_ebb,metricStr)
            
            metadata['tideState'] = 'Ebb only'
            fig_metrics_ebb = plot_validation_stats(obs,mod,metrics_ebb,displayStr,unitStr,min_val,max_val,metadata)
    
            del mod, obs, good
    
            # Store results        
            metrics[paramStr]['all'] = metrics_all
            metrics[paramStr]['flood'] = metrics_fld
            metrics[paramStr]['ebb'] = metrics_ebb
            figHnds[paramStr]['valid_all'] = fig_metrics_all
            figHnds[paramStr]['valid_flood'] = fig_metrics_fld
            figHnds[paramStr]['valid_ebb'] = fig_metrics_ebb

            # Clean up variable space
            del t_obs, v_obs, t_mod, v_mod 
            del metrics_all, metrics_fld, metrics_ebb
        
        else:
            print('  Warning: Parameter '+paramStr+' not available\n    No validation metrics calculated.')
            
    return metrics, figHnds

# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

