# -*- coding: utf-8 -*-
"""
Tools for manupulating GMsh files and converting to different model foramts.
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import numpy as np
# Non-Standard Python Dependencies
import meshio
import metpy.interpolate as interp
# Local Module Dependencies
from fastwater.qmesh.gradraster import ncread
from fastwater.otm_gpl.parser_gmsh import MSH
from fastwater.otm_gpl.selafin import Selafin
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

def get_bathy(bathyfile,boffsets=[0.0,0.0,0.0]):
    fname, fextn = os.path.splitext(bathyfile)
    if fextn.lower() == '.xyz':
        bathy = np.genfromtxt(bathyfile, delimiter=',')
    elif fextn.lower() == '.nc':
        x = ncread(bathyfile,'x')[:].data
        dx = np.nanmean(np.diff(x))
        x = x + boffsets[0]*dx
        print('Bathy x range: ',np.min(x),np.max(x))
        y = ncread(bathyfile,'y')[:].data
        dy = np.nanmean(np.diff(y))
        y = y + boffsets[1]*dy
        print('Bathy y range: ',np.min(y),np.max(y))
        #z = ncread(bathyfile,'Band1')[:].data.transpose()
        z = ncread(bathyfile,'Band1')[:].data
        z = z + boffsets[2]
        print('Bathy data shape: ',z.shape)
        print('Bathy data range: ', np.min(z),np.max(z))
        bx, by = np.meshgrid(x,y)
        px = bx.reshape((np.prod(bx.shape),))
        py = by.reshape((np.prod(by.shape),))
        pz = z.reshape((np.prod(z.shape),))
        bathy = np.asarray((px,py,pz)).transpose()
        print('New bathy data shape: ',bathy.shape)
        print('New bathy data range: ', np.min(bathy[:,2]),np.max(bathy[:,2]))
    return bathy

def interp_bathy(nodes,bathy,radius):
    z = interp.inverse_distance_to_points(bathy[:,0:2],
                                          bathy[:,2],
                                          nodes,
                                          radius,
                                          min_neighbors=2)
    return z

def get_mesh_nodes(inmeshfile):
    mesh = meshio.read(inmeshfile.replace('\\','/'))
    nodes = mesh.points[:,0:2]
    return nodes

def istag(astr):
    flag =  astr[0] == '$'
    return flag

def isscalar(astr):
    flag = len([pos for pos, char in enumerate(astr) if char == ' ']) == 0
    return flag

def mshbathy(datapath,meshfile,bathyfile,newmeshfile,interp_radius,boffsets=[0.0,0.0,0.0]):
    nodes = get_mesh_nodes(datapath+'/'+meshfile)
    bathy = get_bathy(datapath+'/'+bathyfile)
    z = interp_bathy(nodes,bathy,interp_radius)
    fmsh = open(datapath+'/'+meshfile.replace('\\','/'),'r')
    fmshout = open(datapath+'/'+newmeshfile.replace('\\','/'),'w',newline='')
    nnode = 0
    for line in fmsh:
        if istag(line[:-1]):
            tagname = line[:-1]
            print(tagname)
        elif (tagname == '$Nodes') & (not isscalar(line[:-1])):
            vals = line[:-1].split(' ')
            line = ' '.join((vals[0],vals[1],vals[2],str(z[nnode]),'\n'))
            nnode += 1
        fmshout.writelines(line)
    fmsh.close()
    return

def msh2slf(datapath,meshfile,slftitle=''):
    mesh = MSH(datapath+'/'+meshfile)
    head, _ = os.path.splitext(meshfile)
    slf_file = datapath+'/'+head+'.slf'
    mesh.put_content(slf_file)
    # Correct title bug
    slf = Selafin(slf_file)
    slf.title = "{:<80}".format(slftitle)
    # Remove Bathymetry units
    slf.varunits = ['                ']
    # Correct iparam
    slf.iparam = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    # Correct ipob2 & ipob3
    #slf.ipob2[:] = 0
    #slf.ipob3[:] = 0
    # Update file
    slf.put_content(slf_file.replace('_bathy.','.'))
    return
