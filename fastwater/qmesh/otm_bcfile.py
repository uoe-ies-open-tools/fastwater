# -*- coding: utf-8 -*-
"""
Tools for generating an OpenTelemac boundary condtions file (*.cli) from a 
Gmsh that has the bathymetry (z level) data included.

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import numpy as np
# Non-Standard Python Dependencies
# Local Module Dependencies
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------

# Boundary Conditions
# 2 2 2 = Closed boundary (wall)
# 4 4 4 = Open boundary with free H and free UV
# 4 5 5 = Open boundary with free H and prescribed Q
# 5 4 4 = Open boundary with prescribed H and free UV
# 5 5 5 = Open boundary with prescribed H and Q
# 4 6 6 = Open boundary with free H and prescribed UV
# 5 6 6 = Open boundary with prescribed H and UV
# 1 1 1 = Open boundary with incident Waves
bnd_cond_options = ['2 2 2','4 5 5','5 4 4','5 5 5','4 6 6','5 6 6','4 4 4','1 1 1']

# Tracer Boundary Conditions
# 2 = Closed boundary (wall)
# 4 = Open boundary with free Tracer
# 5 = Open boundary with prescribed Tracer
bnd_trcr_options = ['2','5','4']

bnd_dflt_vals = '0.000 0.000 0.000 0.000'
trc_dflt_vals = '0.000 0.000 0.000'

comment_str = '  #'


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

def parse_arguments():
    """
    Parse the command-line arguments.

    Parameters
    ----------

    Returns
    -------
    arguments : Name-space
        A python name-space containing all the command-line arguments
    
    """
    import argparse
    parser = argparse.ArgumentParser(prog="qmesh_otclifile",
                                     description="Generate Open Telemac BC file:"+
                                     " Convert qmesh *.msh file to OT *.cli file")
    parser.add_argument("-v", "--verbosity",
                        action="store",
                        type=str,
                        default='warning',
                        choices=['debug', 'info', 'warning', 'error', 'critical'],
                        help="Level of console verbosity.")
    parser.add_argument("-c", "--cache-images",
                        action="store_true",
                        default=False,
                        help="Retain docker images built for user-specific container.")

    arguments = parser.parse_args()
    return arguments

def istag(astr):
    """
    Check whether the GMsh file line as an ASCII string is a tag.

    Parameters
    ----------
    astr : string
        Single line from a GMsh ASCII file.

    Returns
    -------
    flag : boolean
        flag is set to True if the line is a tag, otherwise set to False.

    """
    flag =  astr[0] == '$'
    return flag

def isscalar(astr):
    """
    Determine if data from the GMsh file as a single (scalar) value or an array.

    Parameters
    ----------
    astr : string
        Single line from a GMsh ASCII file.

    Returns
    -------
    flag : boolean
        flag is set to True is there is a single value on the line otherwise 
        it is set to False.

    """
    flag = len([pos for pos, char in enumerate(astr) if char == ' ']) == 0
    return flag

def initialise_bc_file(datapath,clifile):
    """
    Initialise the OpenTelemac oupt boundary conditions file to be generated.

    Parameters
    ----------
    datapath : string
        Full path string to where the file is to be generated.
    clifile : string
        Name of the OpenTelemac boundary conditions file to generate.

    Returns
    -------
    fhnd : file object
        Handle to the open file object.

    """
    fhnd = open(datapath+'/'+clifile,'w',newline='')
    return fhnd

def clistr(bndcond,trctype,node,elem,bndname):
    """
    Generate output string for curent boundary node to write to the 
    OpenTelemac boundary conditions file.

    Parameters
    ----------
    bndcond : string
        Space delimited 3 digit string defining the boundary condition type. 
        The dafault is '5 6 6' - open bounddary with prescribed UV and H.
    trctype : string
        A single character string defining the tracer condition for the 
        current boundary node. The dafault is '4' Free tracers.
    node : string
        Model boundary node ID as a string.
    elem : string
        Sequential counter value for node element in boundary conditions file.
    bndname : string
        Open boundary name identifier.

    Returns
    -------
    line : string
        Formmated line to write to the OpenTelemac boundary conditions file.

    """
    line = ' '.join([bndcond,
                     ' ',
                     bnd_dflt_vals,
                     trctype.rjust(3),
                     ' ',
                     trc_dflt_vals,
                     node.rjust(11),
                     elem.rjust(11),
                     comment_str,
                     bndname,
                    '\n'])
    return line

def sort_bnds(bnds):
    bndarr = np.asarray(bnds)
    bndids = np.unique(bndarr[:,3])
    bnds_sort = []
    for idx, id in enumerate(bndids):
        indx = np.where(bndarr[:,3]==bndids[idx])[0]
        for i in indx:
            bnds_sort.append(bnds[i])
    return bnds_sort

def extract_mshbnds(datapath,mshfile):
    fmsh = open((datapath+'/'+mshfile).replace('\\','/'),'r')
    nelems = 0
    for line in fmsh:
        if istag(line[:-1]):
            tagname = line[:-1]
            print(tagname)
        elif (tagname == '$Elements') & (not isscalar(line[:-1])):
            vals = list(eval(line[:-1].replace(' ',',')))
            if vals[1] == 1:
                if nelems == 0:
                    bnds = [vals]
                else:
                    bnds.append(vals)
                nelems += 1
    mshbnds = sort_bnds(bnds)
    fmsh.close()
    return mshbnds

def extract_bnd_nodes(mshbnds,bndids):
    bnds = np.asarray(mshbnds)
    bids = bnds[:,3]
    bndnodes = []
    for id in bndids:
        mshindx = np.where(bids==id)[0]
        nodeindx = bnds[mshindx,5].tolist()
        nodeindx.append(bnds[mshindx[-1],6])
        for nidx in nodeindx:
            bndnodes.append([id,nidx])
    return bndnodes

def create_bc_file(datapath,mshfile,clifile,bndids=[1],bndconds=['5 6 6'],trcconds=['4'],bndnames=['openBoundary']):
    fcli = initialise_bc_file(datapath,clifile.replace('_bathy.','.'))
    mshbnds = extract_mshbnds(datapath,mshfile)
    bndnodes = extract_bnd_nodes(mshbnds,bndids)
    ielem = 0
    for vals in mshbnds:
        ielem += 1
        node = str(vals[5])
        elem = str(ielem)
        bndcond = '2 2 2'
        trctype = '2'
        bndname = ''
        if vals[5] in np.asarray(bndnodes)[:,1]:
            indx = np.where(np.asarray(bndnodes)[:,1]==vals[5])[0][0]
            bndid = bndnodes[indx][0]
            bcindx = np.where(np.asarray(bndids)==bndid)[0][0]
            bndcond = bndconds[bcindx]
            bndname = bndnames[bcindx]
            trctype = trcconds[bcindx]
        cliline = clistr(bndcond,trctype,node,elem,bndname)
        fcli.writelines(cliline)
    
    fcli.close()
    return

def msh2cli(datapath, mshfile, bc_suffix=None,bndids=[1],bndconds=['5 6 6'],trcconds=['4'],bndnames=['openBoundary']):
    head, _ = os.path.splitext(mshfile.replace('_bathy.','.'))
    if bc_suffix is None:
        clifile = head+'_BC.cli'
    else:
        clifile = head+'_'+bc_suffix+'_BC.cli'
    create_bc_file(datapath,mshfile,clifile,bndids,bndconds,trcconds,bndnames)
    return
    
