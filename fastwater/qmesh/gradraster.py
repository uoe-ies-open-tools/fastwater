# -*- coding: utf-8 -*-
"""
Mesh refinement functions for QMESH mesh gradation raster generation.

:Dependencies [External]: numpy
:Dependencies [Internal]: 

"""
# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import json
from shutil import copyfile
# Non-Standard Python Dependencies
import numpy as np
from scipy.interpolate import griddata, interp2d
from osgeo import ogr
from netCDF4 import Dataset
import matplotlib.pyplot as plt
from matplotlib import cm, colors, colormaps
from mpl_toolkits.axes_grid1 import make_axes_locatable
# Local Module Dependencies
# Other Dependencies

# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------
parula_vals = [[0.2422    , 0.1504    , 0.6603    , 1.        ],
       [0.25039048, 0.16499524, 0.70761429, 1.        ],
       [0.25777143, 0.18178095, 0.7511381 , 1.        ],
       [0.26472857, 0.19775714, 0.79521429, 1.        ],
       [0.27064762, 0.21467619, 0.83637143, 1.        ],
       [0.27511429, 0.2342381 , 0.87098571, 1.        ],
       [0.2783    , 0.25587143, 0.89907143, 1.        ],
       [0.28033333, 0.27823333, 0.9221    , 1.        ],
       [0.2813381 , 0.30059524, 0.94137619, 1.        ],
       [0.28101429, 0.32275714, 0.95788571, 1.        ],
       [0.27946667, 0.34467143, 0.97167619, 1.        ],
       [0.27597143, 0.36668095, 0.98290476, 1.        ],
       [0.26991429, 0.3892    , 0.9906    , 1.        ],
       [0.26024286, 0.41232857, 0.99515714, 1.        ],
       [0.24403333, 0.43583333, 0.99883333, 1.        ],
       [0.22064286, 0.46025714, 0.99728571, 1.        ],
       [0.19633333, 0.48471905, 0.98915238, 1.        ],
       [0.18340476, 0.50737143, 0.97979524, 1.        ],
       [0.17864286, 0.52885714, 0.96815714, 1.        ],
       [0.1764381 , 0.54990476, 0.95201905, 1.        ],
       [0.16874286, 0.5702619 , 0.93587143, 1.        ],
       [0.154     , 0.5902    , 0.9218    , 1.        ],
       [0.14602857, 0.60911905, 0.90785714, 1.        ],
       [0.13802381, 0.62762857, 0.89729048, 1.        ],
       [0.12481429, 0.64592857, 0.88834286, 1.        ],
       [0.11125238, 0.6635    , 0.87631429, 1.        ],
       [0.09520952, 0.67982857, 0.85978095, 1.        ],
       [0.06887143, 0.69477143, 0.83935714, 1.        ],
       [0.02966667, 0.70816667, 0.81633333, 1.        ],
       [0.00357143, 0.72026667, 0.7917    , 1.        ],
       [0.00665714, 0.73121429, 0.76601429, 1.        ],
       [0.04332857, 0.74109524, 0.73940952, 1.        ],
       [0.09639524, 0.75      , 0.7120381 , 1.        ],
       [0.14077143, 0.7584    , 0.68415714, 1.        ],
       [0.1717    , 0.7669619 , 0.65544286, 1.        ],
       [0.19376667, 0.77576667, 0.6251    , 1.        ],
       [0.21608571, 0.7843    , 0.5923    , 1.        ],
       [0.24695714, 0.79179524, 0.55674286, 1.        ],
       [0.29061429, 0.79729048, 0.51882857, 1.        ],
       [0.34064286, 0.8008    , 0.47885714, 1.        ],
       [0.3909    , 0.80287143, 0.43544762, 1.        ],
       [0.44562857, 0.80241905, 0.39091905, 1.        ],
       [0.5044    , 0.7993    , 0.348     , 1.        ],
       [0.5615619 , 0.79423333, 0.30448095, 1.        ],
       [0.61739524, 0.78761905, 0.2612381 , 1.        ],
       [0.67198571, 0.77927143, 0.2227    , 1.        ],
       [0.7242    , 0.76984286, 0.19102857, 1.        ],
       [0.77383333, 0.75980476, 0.16460952, 1.        ],
       [0.82031429, 0.74981429, 0.15352857, 1.        ],
       [0.86343333, 0.7406    , 0.15963333, 1.        ],
       [0.90354286, 0.73302857, 0.17741429, 1.        ],
       [0.93925714, 0.72878571, 0.20995714, 1.        ],
       [0.97275714, 0.72977143, 0.23944286, 1.        ],
       [0.99564762, 0.74337143, 0.23714762, 1.        ],
       [0.99698571, 0.76585714, 0.21994286, 1.        ],
       [0.99520476, 0.78925238, 0.2027619 , 1.        ],
       [0.9892    , 0.81356667, 0.18853333, 1.        ],
       [0.97862857, 0.83862857, 0.17655714, 1.        ],
       [0.96764762, 0.8639    , 0.16429048, 1.        ],
       [0.96100952, 0.88901905, 0.15367619, 1.        ],
       [0.95967143, 0.91345714, 0.14225714, 1.        ],
       [0.96279524, 0.9373381 , 0.12650952, 1.        ],
       [0.96911429, 0.96062857, 0.1063619 , 1.        ],
       [0.9769    , 0.9839    , 0.0805    , 1.        ]]

# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------
def ncread(filename, varname):
    with Dataset(filename,'r') as nc:
        if varname in list(nc.variables):
            if np.ma.isMaskedArray(nc.variables[varname]):
                var = np.ma.getdata(nc.variables[varname][:])
            else:
                var = nc.variables[varname][:]
        else:
            var = None
    return var

def ncwrite(filename, varname, var):
    with Dataset(filename,'a') as nc:
        if varname in list(nc.variables):
            nc.variables[varname][:] = var
    return var

def writeRasterFile(rasterPath,baseRasterFile,rasterFile,raster):
    copyfile(rasterPath+'/'+baseRasterFile,rasterPath+'/'+rasterFile)
    ncwrite(rasterPath+'/'+rasterFile,'z',raster)
    return

def getRasterCoords(rasterPath,rasterFile):
    xm = ncread(rasterPath+'/'+rasterFile,'x')
    ym = ncread(rasterPath+'/'+rasterFile,'y')
    [xq, yq] = np.meshgrid(xm,ym)
    return [xm,ym,xq,yq]

def normalise(v):
    vnorm = v - np.nanmin(v)
    vnorm = vnorm / np.nanmax(vnorm)
    return vnorm

def gradate(minval,maxval,var):
    scl = var - np.nanmin(var[:])
    delta = maxval-minval
    grade = (delta/np.nanmax(scl[:]))*scl+minval;
    return grade

def distance2coast(shpPath,shpFile,BndIDs,x,y):
    sfile = ogr.Open(shpPath+'/'+shpFile)
    shapes = sfile.GetLayer(0)
    
    nLines = len(shapes)
    edgeX = None
    edgeY = None

    for ibnd in range(nLines):
        feature = shapes.GetFeature(ibnd)
        ID = feature['PhysID']
        if ID in BndIDs:
            geom = json.loads(feature.ExportToJson())
            coords = np.asarray(geom['geometry']['coordinates'])
            if edgeX is None:
                edgeX = coords[:,0].copy()
                edgeY = coords[:,1].copy()
            else:
                edgeX = np.concatenate((edgeX,coords[:,0]))
                edgeY = np.concatenate((edgeY,coords[:,1]))
    
    dist = np.empty(x.shape)
    for pt in range(len(x)):
        dx = edgeX - x[pt]
        dy = edgeY - y[pt]
    
        dr = np.sqrt(np.square(dx) + np.square(dy))
        
        dist[pt] = np.min(dr)
    
    return dist

def rasterise(x,y,v,xq,yq):
    xm = xq[0,:]
    ym = yq[:,0]
    dx = np.mean(np.diff(xm))
    dy = np.mean(np.diff(ym))
    raster = griddata((x,y),v,(xq,yq),'linear')
    return raster

def normalise(v):
    vnorm = v - np.nanmin(v)
    vnorm = v / np.nanmax(vnorm)
    return vnorm

def vargradient(x,y,v,xq,yq):
    xm = xq[0,:]
    ym = yq[:,0]
    dx = np.mean(np.diff(xm))
    dy = np.mean(np.diff(ym))
    vgrid = griddata((x,y),v,(xq,yq),'linear')
    [dvdx,dvdy] = np.gradient(vgrid,dx,dy)
    vgrad = np.sqrt(np.square(dvdx)+np.square(dvdy))
    #ifunc = interp2d(xm,ym,grad,kind='linear')
    #vgrad = ifunc(x,y)
    return vgrad

'''
def plotMesh(msh,titleStr):
    import matplotlib.pyplot as plt
    fig = plt.figure(figsize=(9,5))
    #fig.set('position',[300 50 800 450])
    ax = plt.axes(projection ='2d',aspect='equal') 
    meshsize = length(msh.meshx)
    plt.trimesh(msh.elems,msh.meshx,msh.meshy,ones(size(msh.meshx)))
    view(2)
    plt.xlim([0,36])
    plt.ylim([0,19])
    plt.title(titleStr+' - Mesh')
    plt.xlabel('X  [ m ]')
    plt.ylabel('Y  [ m ]')
    return

def plotMeshRasterVals(msh,vals,titleStr):
    fig = figure(figsize=(9,5),aspect=1);
    meshsize = length(msh.meshx);
    h = trisurf(msh.elems+1,msh.meshx,msh.meshy,zeros(meshsize,1),vals,'EdgeColor','none');
    axis equal
    xlim([0,36])
    ylim([0,19])
    colorbar()
    title([titleStr ' - ' 'Raster Data from Velocity Gradient'])
    xlabel('X  [ m ]')
    ylabel('Y  [ m ]')
    return
'''
def gen_parula_cmap():
    parula = colors.ListedColormap(parula_vals)
    return parula

def plotRaster(x,y,raster,minval,maxval):
    cmap = gen_parula_cmap()
    fig = plt.figure(figsize=(9,5))
    ax = fig.gca()
    divider = make_axes_locatable(ax)
    ax_cb = divider.new_horizontal(size="4%", pad=0.1)
    fig = ax.get_figure()
    fig.add_axes(ax_cb)
    #plt.set_cmap('viridis')
    #plt.set_cmap('parula')
    im = ax.imshow(raster,origin='lower',cmap=cmap,extent=(np.min(x),np.max(x),np.min(y),np.max(y)),vmin=minval,vmax=maxval)
    plt.colorbar(im, cax=ax_cb)
    return fig,ax,ax_cb



