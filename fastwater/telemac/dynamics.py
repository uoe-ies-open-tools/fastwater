# -*- coding: utf-8 -*-
"""
"""

#==========================================================================
# Generic geometry tools.
#==========================================================================

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import datetime
import numpy as np
import scipy.io as sio
# Non-Standard Python Dependencies
# Local Module Dependencies
from fastwater.general.fileTools import getListOfFiles
from fastwater.telemac.extraction import enhanceMesh
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

def calcDynamics(mesh,data):
    area = mesh['area'].copy()
    dpth = data['dpth'].copy()
    vel_x = data['velx'].copy()
    vel_y = data['vely'].copy()
    vort = data['vort'].copy()
    nElems = np.shape(mesh['elems'])[0]
    depth = np.zeros([nElems,])
    velX = np.zeros([nElems,])
    velY = np.zeros([nElems,])
    vorticity = np.zeros([nElems,])
    enstrophy = np.zeros([nElems,])
    for ielem in np.arange(nElems):
        elem = mesh['elems'][ielem,:].copy()
        cWghts = mesh['cWghts'][ielem,:].copy()
        depth[ielem] = np.sum(dpth[elem]*cWghts)
        velX[ielem] = np.sum(vel_x[elem]*cWghts)
        velY[ielem] = np.sum(vel_y[elem]*cWghts)
        vorticity[ielem] = np.sum(vort[elem]*cWghts)
        enstrophy[ielem] = vorticity[ielem]*vorticity[ielem]*area[ielem]*depth[ielem]
    dynamics = {}
    dynamics['depth'] = np.float32(depth)
    dynamics['velX'] = np.float32(velX)
    dynamics['velY'] = np.float32(velY)
    dynamics['vorticity'] = np.float32(vorticity)
    dynamics['enstrophy'] = np.float32(enstrophy)
    return dynamics

def processFile(filePath,dataFile,mesh):
    data = sio.loadmat(os.path.join(filePath,dataFile))
    dynamics = calcDynamics(mesh,data)
    matFile = dataFile.replace('gradients','dynamics')
    sio.savemat(os.path.join(filePath,matFile),dynamics)
    procTStamp = datetime.datetime.now().strftime("%d %b %Y %H:%M:%S")
    return matFile,procTStamp

def processSet(filePath,srchStr,nCores,setNum):
    meshfile = getListOfFiles(filePath,'*'+srchStr+'*mesh*.mat')
    mesh = sio.loadmat(os.path.join(filePath,meshfile[0]))
    if not mesh.has_key('area'):
        mesh = enhanceMesh(mesh)
        sio.savemat(os.path.join(filePath,meshfile[0]),mesh)
    dataFiles = getListOfFiles(filePath,'*'+srchStr+'*gradients*STEP*.mat')
    nFiles = len(dataFiles)
    nFilesPerCore = np.ceil(nFiles/nCores).astype(np.int)
    if setNum == nCores-1:
        nProcess = nFiles - nFilesPerCore*(nCores-1)
    else:
        nProcess = nFilesPerCore
    print('Batch Process Set Number = '+str(setNum))
    print('Number of processing cores = '+str(nCores))
    print('Number of files to process on core = '+str(nProcess))
    print('Process started: '+datetime.datetime.now().strftime("%d %b %Y %H:%M:%S"))
    for iFile in np.arange(nProcess):
        fileIndx = iFile+(setNum*nFilesPerCore)
        matFile,procTStamp = processFile(filePath,dataFiles[fileIndx],mesh)
        print(str(iFile).zfill(4)+'  '+matFile+'  '+procTStamp)
    print('Process completed: '+datetime.datetime.now().strftime("%d %b %Y %H:%M:%S"))
    return 

