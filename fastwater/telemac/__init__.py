__path__ = __import__('pkgutil').extend_path(__path__, __name__)
import fastwater.telemac.extraction
import fastwater.telemac.vectorfield
import fastwater.telemac.dynamics
import fastwater.telemac.tidalAnalysis