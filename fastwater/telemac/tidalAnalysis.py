# -*- coding: utf-8 -*-
"""
Harmonic reduction of tidally forced variables from Open Telemac output

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import datetime
# Non-Standard Python Dependencies
import numpy as np
import scipy.io as sio
# Local Module Dependencies
from fastwater.general.fileTools import getListOfFiles,loadmat
from fastwater.telemac.extraction import InitialiseFile, getVariableData
from fastwater.telemac.extraction import Date2Num
from fastwater.analysis.tidal import generateGlobalAttributes
from fastwater.analysis.tidal import harmonicReduction
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

def getSLF(filePath,fileName):
    slf = InitialiseFile(os.path.join(filePath,fileName))
    return slf

def getTimeHeight(slfHnd,node):
    idate = slfHnd.DATETIME
    times = slfHnd.tags['times']
    nstps = np.size(times)
    varNames = slfHnd.VARNAMES
    vals = slfHnd.getSERIES([node+1],showbar=False) # Have to offset Node indices as code assumes numbering from 1 not 0.
    TS_t = np.reshape(Date2Num(idate) + (times / (24.*60.0*60.)),[nstps,1])
    dt = []
    for i in np.arange(len(TS_t)):
        dt.append(datetime.datetime.fromordinal(int(TS_t[i])) + datetime.timedelta(days=(TS_t[i]%1)[0]))
    wl = getVariableData(vals,varNames,'FREE SURFACE    ')
    return dt,wl

def processAllNodes(slfHnd,tidalCons):
    fname = slfHnd.file['name'].split('/')[-1].split('.')[0]+'_harmonics.csv'
    outf = open(fname,'a')
    for node in np.arange(slfHnd.NPOIN3):
        t,h = getTimeHeight(slfHnd,node)
        tide = harmonicReduction(h,t,tidalCons)
        data = np.array([slfHnd.MESHX[node],slfHnd.MESHY[node]])
        data = np.append(data,tide.model[0][1])
        data = np.append(data,[(tide.model[i+1][1],tide.model[i+1][2]) for i in np.arange(len(tide.model)-1)])
        np.savetxt(outf, data.reshape(1,len(data)), delimiter=",")
    outf.close()
    return

def processNodes(fPath,fName,tidalCons,nCores,setNum):
    print('Tidal Harmonic Extraction for Nodes in SLF Model File')
    print('  File: '+fName)
    print('  Num Cores Used: '+str(nCores))
    slfHnd = getSLF(fPath,fName)
    nCons = len(tidalCons)
    if np.mod(slfHnd.NPOIN3,nCores) == 0:
        nNodes = slfHnd.NPOIN3/nCores
        nodeOffset = setNum*nNodes
    else:
        nNodes = slfHnd.NPOIN3/(nCores-1)
        nodeOffset = setNum*nNodes
        if (setNum+1) == nCores:
            nNodes = slfHnd.NPOIN3-(setNum*nNodes)
    print('  Number of Nodes to Process: '+str(nNodes))
    print('  First Node in Set: '+str(nodeOffset))
    print('  Number of Tidal Constituents: '+str(len(tidalCons)))
    matFile = slfHnd.file['name'].split('/')[-1].split('.')[0]+'_harmonics_'+'SET'+str(setNum).zfill(4)+'.mat'
    nVals = nCons*2+1
    data = np.zeros((nNodes,nVals))
    for inode in np.arange(nNodes):
        node = inode+nodeOffset
        t,h = getTimeHeight(slfHnd,node)
        tide = harmonicReduction(h,t,tidalCons)
        data[inode,0] = tide.model[0][1]
        for i in np.arange(len(tide.model)-1):
            indx = 2*i+1
            data[inode,indx] = tide.model[i+1][1]
            data[inode,indx+1] = tide.model[i+1][2]
    glbAttr = generateGlobalAttributes('Tidal Harmonics - '+os.path.split(os.path.splitext(matFile)[0])[1])
    dict = {}
    dict['globalAttributes'] = glbAttr
    dict['indices'] = np.arange(nNodes)+nodeOffset
    dict['harmonics'] = data
    sio.savemat(matFile,dict)
    print('*** PROCESS COMPLETE ***')
    return

def concatenateHarmonics(fPath,searchStr):
    hfiles = getListOfFiles(fPath,searchStr)
    outFile = '_'.join(hfiles[0].split('_')[0:-1])+'.mat'
    harmonics = np.asarray([])
    indices = np.asarray([])
    for afile in hfiles:
        hdata = loadmat(os.path.join(fPath,afile))
        harmonics = np.append(harmonics,hdata['harmonics'])
        indices = np.append(indices,hdata['indices'])
    ncols = hdata['harmonics'].shape[1]
    nrows = harmonics.shape[0]/ncols
    harmonics = harmonics.reshape((nrows,ncols))
    glbAttr = generateGlobalAttributes('Concatenated Tidal Harmonics - '+outFile)
    dict = {}
    dict['globalAttributes'] = glbAttr
    dict['indices'] = indices
    dict['harmonics'] = harmonics
    sio.savemat(outFile,dict)
    return


#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

