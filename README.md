# FASTWATER

[FASTWATER](https://supergen-ore.net/projects/fastwater): Freely Available Simulation Toolset for Waves, Tides and Eddy Replication.

Funded by SuperGen ORE Hub (EP/S000747/1) Flexible Fund Round 3, 2020.

### Purpose
The FASTWATER platform provides a set of core model data sets, development and processing software, and methodologies that support the transfer of academic knowledge and skills across to the ORE sector with the aim of improving and accelerating the development of robust, calibrated and validated regional hydrodynamic and wave models for the Scottish waters, that capture the key physical processes relevant to ORE development.

### Documentation
Current documentation is at [Read the Docs](https://fastwater.readthedocs.io/en/latest/).
The documentation contains a description of the FASTWATER tools, provides two Case Studies showing the application of the software tools, and outlines the methods used and in development for mesh refinement and model cal/val. 

### Scripts
A set of processing scripts are provided that were constructed to process the Case Studies. These are available in the \scripts directory.

### Install package 
For cleaner package management and to avoid conflicts between different versions of packages, we recommend installing inside an Anaconda or pip environment.
However, this is not required.

First, pull down the current source code from either by downloading a zip file or using `git clone`.

Included in the repository is an environment.yml file that can be used to build a conda environment for installing the `fastwater` package. To build the conda environment in an Anaconda shell change to the fastwater directory where the source code has be downloaded, then run the command:

        conda env create -f environment.yml
        
This will build a conda environment called `fwenv`. To check that the environment has been built use:

        conda env list
        
The environment `fwenv` should be in the list. 

To activate this environment use:

        conda activate fwenv

The `fastwater` package is install from the from the command line as follows: while in the main FASTWATER directory, use the command:

        pip install -e .

The `-e` flag signals developer mode, meaning that if you update the code from Github, your installation will automatically take those changes into account without requiring re-installation.
Some other essential packages used in FASTWATER may be installed if they do not exist in your system or environment.

If you encounter problems with the above install method, you may try to install dependencies manually before installing FASTWATER.
First, ensure you have a recent version of Python (greater than 3.5).
Then, install packages `numpy`, `scipy`, `matplotlib`, `netCDF4`, `meshio`, `metpy`, `pytides`, `utm`, `pytictoc`, and `scikit-learn`.        

### Citing FASTWATER
Using FASTWATER in your work? Cite as:

Chris Old, et al.
uoe-ies/FASTWATER. Zenodo. 

---

© 2022. University of Edinburgh. All rights reserved. This software was produced for The University of Edinburgh under the 
SuperGen ORE HUB (EP/S000747/1) Flexible Fund Round 3, 2020. All rights in the software are reserved by the University of Edinburgh. 
The University of Edinburgh is granted, for itself and others acting on its behalf, a nonexclusive, paid-up, irrevocable 
worldwide licence in this material to reproduce, prepare derivative works, distribute copies to the public, perform publicly 
and display publicly, and to permit others to do so.

This software is open source under the BSD-3 Licence. Redistribution and use in source and binary forms, with or without
modification, is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following 
disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
disclaimer in the documentation and/or other materials provided with the distribution. 
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote 
products derived from this software without specific prior written permission. 

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
