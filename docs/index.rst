FASTWATER
=========
.. automodule:: fastwater
    :members:
   :undoc-members:
   :show-inheritance:

.. _FASTWATER: https://git.ecdf.ed.ac.uk/uoe-ies-open-tools/fastwater 
.. _GitLab: https://git.ecdf.ed.ac.uk/uoe-ies-open-tools/fastwater 

.. _numpy: https://pypi.org/project/numpy/
.. _scipy: https://pypi.org/project/scipy/
.. _matplotlib: https://pypi.org/project/matplotlib/
.. _netCDF4: https://pypi.org/project/netCDF4/
.. _meshio: https://pypi.org/project/meshio/
.. _metpy: https://pypi.org/project/MetPy/
.. _pytides: https://pypi.org/project/pytides/
.. _utm: https://pypi.org/project/utm/
.. _pytictoc: https://pypi.org/project/pytictoc/
.. _scikit-learn: https://scikit-learn.org/stable/install.html


What is FASTWATER?
###################

The `FASTWATER`_ platform will provide a set of core model data sets, development and processing software, and 
methodologies that support the transfer of academic knowledge and skills across to the ORE sector with the 
aim of improving and accelerating the development of robust, calibrated and validated regional hydrodynamic 
and wave models for the Scottish waters, that capture the key physical processes which impact ORE development.

Full source code is available from the `GitLab`_ repository held at The University of Edinburgh. 

While `FASTWATER`_ is still under development, users should pull the newest code from the `GitLab`_ 
repository regularly. If you have installed the package using the instructions below, you should not 
need to re-install after pulling new code.

`FASTWATER`_ is still under development and is intended to be a research tool, not production-level code.
Please submit questions as `GitLab`_ issues if you encounter unexpected behaviour or need help using 
`FASTWATER`_. 

Installation
-------------

First, pull down the current source code from `GitLab`_ either by downloading a zip file or using `git clone`.

From the command line, while in the main FASTWATER directory, use the following command to install `FASTWATER`_::

        pip install -e .

The `-e` flag signals developer mode, meaning that if you update the code from `Gitlab`_, your installation will 
automatically take those changes into account without requiring re-installation.
Some other essential packages used in `FASTWATER`_ may be installed if they do not exist in your system or environment.

If you encounter problems with the above install method, you may try to install dependencies manually before 
installing `FASTWATER`_. First, ensure you have a recent version of Python (greater than 3.5).
Then, install packages `numpy`_, `scipy`_, `matplotlib`_, `netCDF4`_, `meshio`_, `metpy`_, `pytides`_, `utm`_, `pytictoc`_, and `scikit-learn`_.


FASTWATER Platform
###################
.. toctree::
    :maxdepth: 2

    about
    background
    meshdesign
    calvalmethods
    basemodel
    casestudies
    software
    datasets

FASTWATER Package Content
##########################

.. toctree::
   :maxdepth: 3

   fastwater.general
   fastwater.analysis
   fastwater.telemac
   fastwater.qmesh
   fastwater.otm_gpl
   
.. only:: html

    Indices and tables
    ###################
    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
