fastwater.otm\_gpl package
==========================

.. automodule:: fastwater.otm_gpl
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   fastwater.otm_gpl.exceptions
   fastwater.otm_gpl.files
   fastwater.otm_gpl.geometry
   fastwater.otm_gpl.parser_gmsh
   fastwater.otm_gpl.parser_kenue
   fastwater.otm_gpl.parser_selafin
   fastwater.otm_gpl.progressbar
   fastwater.otm_gpl.selafin
