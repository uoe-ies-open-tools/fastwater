.. _aboutfastwater:

About FASTWATER
===============

The FASTWATER platform will provide a set of core model data sets, development and processing software, and methodologies 
that support the transfer of academic knowledge and skills across to the ORE sector with the aim of improving and 
accelerating the development of robust, calibrated and validated regional hydrodynamic and wave models for the Scottish 
waters, that capture the key physical processes which impact ORE development.

The FASTWATER code is developed and maintained by the IES, School of Engineering, at The University of Edinburgh, UK.

Licence
#######

© 2022. University of Edinburgh. All rights reserved. This software was produced for The University of Edinburgh under the 
SuperGen ORE HUB (EP/S000747/1) Flex Fund Round 3, 2020. All rights in the software are reserved by the University of Edinburgh. 
The University of Edinburgh is granted, for itself and others acting on its behalf, a nonexclusive, paid-up, irrevocable 
worldwide licence in this material to reproduce, prepare derivative works, distribute copies to the public, perform publicly 
and display publicly, and to permit others to do so.

This software is open source under the BSD-3 Licence. Redistribution and use in source and binary forms, with or without
modification, is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following 
   disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
   disclaimer in the documentation and/or other materials provided with the distribution. 
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote 
   products derived from this software without specific prior written permission. 

**Disclaimer**

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.