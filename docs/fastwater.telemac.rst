fastwater.telemac package
=========================

.. automodule:: fastwater.telemac
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   fastwater.telemac.dynamics
   fastwater.telemac.extraction
   fastwater.telemac.tidalAnalysis
   fastwater.telemac.vectorfield
