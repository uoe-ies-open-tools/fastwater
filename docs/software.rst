.. _software:


Software Tools
==============


FASTWATER Modules
------------------


General Tools
~~~~~~~~~~~~~~


Analysis Tools
~~~~~~~~~~~~~~~


QMesh Tools
~~~~~~~~~~~~


OpenTelemac Tools
~~~~~~~~~~~~~~~~~~


Processing Scripts
-------------------


Data Extraction
~~~~~~~~~~~~~~~~


Applied Cal/Val
~~~~~~~~~~~~~~~~


Applied Analysis
~~~~~~~~~~~~~~~~~


Meshing Applications
~~~~~~~~~~~~~~~~~~~~~


