.. _datasets:


Open Data Sets
==============


Location, extraction and conversion of data for model construction is time consuming and can be a barrier to model imrprovement. To help fast-track model development, a set of pre-built data are provided that cover the Orkney waters to beyond the Fair Ilses, out onto the Western shelf and into the North Sea. The core datasets are bathymetric data derived from EMODNet archives and nominally corrected to MSL, and spatailly varying bottom friction coefficient sets derived from the EMODNet EUSM2019 substrate class maps. The various files used to run the OpenTelemac models are included so that they can be reused and modified for other modelling requirements. Model output associated with the some of the OpenTelemc model runs are provided for post-anaysis and use as input to other process models. The data are accessible through the 

Overview
########


