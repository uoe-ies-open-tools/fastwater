fastwater.general.substrates module
===================================

.. automodule:: fastwater.general.substrates
   :members:
   :undoc-members:
   :show-inheritance:
