.. _background: 

FASTWATER Background
====================

The generation of regional scale tidal models for use in characterisation of tidal energy sites requires a certain level of expertise and can be time consuming to develop from scratch. Based on feedback from industrial partners in previous projects (ReDAPT, RealTide, ResourceCode), accessbility to the core tools and data needed to reliably interpret and utilise regional model output data a limiting factor in the development of the tidal energy sector. Similarly, access to base model constructs and boundary condition data accelerates the application of regional modelling to engineering problems by graduate students, minimising the amount of time spent trying to construct suitable models, and allowing greater focus on solving the problem posed. The aim has also been to focus on the use of open-access tools, as this allows a significantly larger group of end-users to take advantage of the FASTWATER outputs.

The main goal of the FASTWATER project is to provide a step toward making regional modeling more accessible through the provision of basic model constructs, pre-generated boundary conditions data, and a set of software tools that can be used to calibrate, validate and post-process model data in a standardised manners. The tools have been developed based on regional models developed using the Open Telemac-Mascaret hydrodynamic solver software. The intent is the make the post-processing as model independent as possible through the extraction and conversion of model data into standardised data structures that are passed to the processing tools. The main caveat to this statements is that the tools are currently only work for unstructured triangular meshes.

What is a Regional Tidal Model?​
--------------------------------

A regional tidal model is a numerical approximation, both geometrically and physically, to a real-world tidally affected marine region.​

The underlying physical processes are represented by a set of non-linear time-evolving differential equations (Navier-Stokes Equations + Free-Surface Potential Equation) that are solved numerically within the boundary condition constraints imposed by the geometry, forcing, and underlying physical laws for fluid behaviour when represented as a continuum.​

A given model construct contains a subset of all possible real-world processes (e.g. tidal forcing, bottom friction, internal friction, Coriolis forcing, etc.).​ Depending on the required end-use, these models can represent the spatial domain using either a 2-D or 3-D discretised mesh. The spatial resolution of discretisation is also defined by the intended purpose of the model.

The mesh discretisation inherently imposes spatial filter at the imposed length scales on the physical process. Some of the key physical processes operate on subgrid-scale length scales, therefore the parameterisation of these subgrid-scale processes (e.g. turbulence, mixing, diffusion, etc.) is required for closure of the system of differential equations being solved.​

A given model time step represents an instance of the modelled system state within the limits of the numerical construct.​ Generally data can not be stored for every model time step due to data volume constraints. A subset of time steps is typically stored, e.g. the model time step may be 1s, but data are stroed every 5 minutes. This can lead to sampling based aliasing in the stored data.

What are observational measurements?
-------------------------------------

Observational data are discrete measurements of physical features (e.g. fluid velocity, fluid pressure, free-surface elevation, etc.) of the real-world environment.

The measurements may or may not be temporally and/or spatially averaged.

The information content within the measurements is constrained by instrument accuracy, precision, sampling rate, and any length-scale constraints imposed by the instrumetn design.

Data quality is constrained by the level of quality assurance (QA) applied prior to collection (experimental design, deployment methods, instrument calibration, instrument maintenance, etc.), identification of incorrect measurements (QC) during post-processing, identification and quantification of measurement uncertainty, and the traceability of applied post-processing methods (e.g. verified post-processing software tools with a defined algorithm basis).

Each discrete measurement contains information about the real system state at the measurement location for the given instance in time. Typically there will more processes measured than can be represented by a numerical simulation. Most oceanographic instruments can store high-frequency data over long deployments, or can run internal post-processing to impose some temporal averaging. For this reason in situ data are often less prone to sampling alias issues.

Physical Processes
-------------------

The physical processes that are relevant to a high energy tidal energy site can be broken down into the fundamental tidal forcing that provides the majority of the system energy, non-tidal forcing processes that contribute to the system forcing, and tidal and non-tidal system response to the forcing. It is the system responses to the forcing that provides the conditions of enhanced energy density that are being exploited. These physical processes are briefly outline below. 

Tidal forcing​
~~~~~~~~~~~~~~

The tidal energy comes from the varying geopotential energy associated with the gravitational variations of the Earth-Moon-Sun three-body system. ​

The orbital dynamics of this three-body system are predictable, therefore the corresponding equilibrium tide is also predictable.​

The oceanic tidal wave generated by the varying geopotential surface induces tidal currents in marine fluids.​ There is a corresponding Earth tide in the Earth's crust, but this is generally ignored for regional scale marine systems.

The interaction of the tidal wave with shallow-water bathymetry and land masses leads to distortions in the wave form and propagation direction, and in the associated tidal currents generated in the fluid body. ​

Bottom friction leads to the local transfer of energy into higher harmonics of the fundamental tidal forcing frequencies, these higher harmonics vary depending on location.​

Tidal energy extraction sites take advantage of the resulting flow acceleration due to flow constriction or curvature.​

Non-tidal forcing
~~~~~~~~~~~~~~~~~~

Atmospheric pressure alters the free-surface hydraulic adjustment over scale comparable to those of weather systems, this effects is most pronounced for intense low-pressure weather systems.

Surface winds generate local waves, surface Ekman layers, and can generate hydraulic heads when opposing the direction of the tidal flow.

Remotely generated storm swell can impact on coastal tidal energy extraction sites where the effects of bathymetry shorten the wave-length and increase the amplitude of these surface gravity waves.

Storm and wind generated surface gravity waves interact non-linearly with the tidal currents; there is a two-way coupling to these interactions where both the waves and underlying currents are modified.

Freshwater input from streams and rivers impart momentum to the fluid at the point of entry into the region, and the presence of fresh water alters the local fluid density resulting in the formation of baroclinic processes. In areas of strong vertical density gradients, tidal straining processes can dominate the vertical velocity structures.

Tidal system response
~~~~~~~~~~~~~~~~~~~~~~

The dominant tidal forcing comes from the gravitational forcing associated with the Moon. This tidal forcing leads to a raising of the sea surface beneath the Moon and on the diametrically opposite point on the Earths surface. The combination of the orbital period of the Moon around the Earth and diurnal rotation of Earth leads to a dominant lunar period of 12.42 hours, this is typically identified as the M2 tidal constituent. This forcing leads to two high-low tide cycles per day, with an approximate shift of +50 minutes in the timing of the tides between days. Leading to a cyclic return every 4 weeks, leading to 13 lunar months in a year. 

The Sun is the other dominant gravitational forcing body affecting the Earth's ocean tides. The solar tides operate at the half-diurnal period of 12 hours. Over the lunar monthly cycle there is shift in the relative loction of the Earth-Moon-Sun orientation. This shift leads to a fortnightly neap-spring tidal cycle, where the spring tides coincide with a full Moon and New Moon (i.e. Earth-Moon-Sun system is aligned), and the neap tides occur when the Moon and Sun are in quarature with each other, coinciding with the half-Moon waxing or waning.

When the sea surface is raised water needs to move toward the local high point for conservation of mass, leading to the formation of tidal currents running in opposite directions on either side of the high-tide location. On the open shelf away from land the currents are out of phase by quadrature with the surface forcing, i.e. at high and low water the tidal currects are zero, whereas the maximum flow occurs when the surface elevation passes through the mean sea level point.

This is a very simplistic view of the tidal processes. The presence of land mass and subsea topographic form means that the spherical harmoincs that describe the tidal forcing are spatiall distorted, leading to a shft in the timing of high and low water relative to the passage of the Moon over the local meridian. The physical barrier formed by land mass above the mean sea level locally distorts the tidal flow symmetry between flood (rising tidal level) and ebb (falling tidal level) tides.

Non-tidal system response
~~~~~~~~~~~~~~~~~~~~~~~~~~

Free-surface fluid flow past obstacles creates local surface distortions in teh vicinity of teh obstacles. These distortions represent a hydraulic adjustment of the fluid for energy balance. 

Surface distortions lead to the generation of horizontal flow in the direction of the surfae slope. This is secondary flow often at an angle to the dominant flow direction, resulting in the foramtion of horizontal fluid shear and flow rotation.

The presence of flow rotation typically leads to the formation of large-scale coherent structures (eddies), which become 2-D once the lengths scales exceed the water depth.

Eddies can be trapped to or separate from the generation point, and they extract energy from the dominant flow by converting linear momentum into angular momentum. Their persistence is associated with a local surface deformation centred on the eddies core.

Flow curvature generates secondary circulation in the vertical plane through the fluid, this results in large-scale helical flow along channels and upwelling at headlands.

Bed friction creates a bottom boundary layer in the flow, resulting in a shear velocity structure in the fluid velocity field from the bed to the surface. In simple cases this can be represented by a log-law profile, however as previously mentioned surface wind can also generate a near-surface Ekman layer which will distort this profile form. Also the presence of persitent eddy structures can lead to a local distortion of this simple log-law form.

Rollers generated at the bed through bottom friction generated shear processes, propagate in the direction of the dominant flow and interact with the varying bathymetry. These rollers can detach from the bed forming vertical burst of vorticity (typically seen as surface boils).

Calibration and Validation
---------------------------

What is Model Calibration?
~~~~~~~~~~~~~~~~~~~~~~~~~~~

What is Model Validation?
~~~~~~~~~~~~~~~~~~~~~~~~~~

Validation is the quantification of how well numerical simulations predict the time-evolving system state of real-world physical processes when compared to observational measurements (in situ or remote).

The level of agreement required is defined by the end-use purpose of the simulated data, i.e. is the model construct fit-for-purpose.

Quantification is typically provided by a set of statistical metrics based on the calculated differences between predicted and observed system state parameters.

How representative the validation is depends on the spatial and temporal coverage of the observational data used, i.e. how many different regions of the model domain are sampled by the data and how many of the key forcing periods (or frequencies) are resolved by the time series of discrete measurements.

How reliable the validation is depends on the quantification metrics used and the physical parameters chosen for the validation exercise.

Sources of Model Uncertainties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Model type – 2-D depth averaged hydrostatic vs 3-D non-hydrostatic
- Coordinate reference system – spherical vs transverse Mercator
- Coastline – resolution, inclusion of small islands, inlets, complex headlands, etc.
- Bathymetry – resolution and accuracy of bathymetric data, bathymetric datum and correction to MSL
- Tidal forcing – number of harmonics, accuracy of tidal atlas used
- Bottom roughness – constant vs spatially varying, capture of relevant length scales
- Non-tidal processed – wind stress, atmospheric pressure, surface gravity waves density variations
- Wave-current coupling – one-way, two-way, non-linear processes included
- Turbulence closure scheme – constant viscocity, mixing length, k-ε, k-ω, Smagorinsky, DES
- Numerical solving schemes – explicit, semi-implicit, implicit, numerical scheme convergence precision
- Time step – solution stability (convergence), Courant number stability

Sources of Measurement Uncertainties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Time – synchronisation, time zone, clock-drift
- Heading – compass calibration, heading correction for mooring frame, local magnetic anomaly
- Motion – instrument pitch, roll, heave
- Accuracy – instrument configuration, calibration, sensor accuracy, measurement bias, data conversion
- Precision – sensor sensitivity
- Location – accuracy of mooring position, movement of mooring
- Instrument Design
	- ADCP: divergent vs convergent, assumptions of spatial homogeneity in field
	
	- Altimetry: flat footprint vs along-track SAR
	
	- Waves: pressure sensor, wavebuoy, acoustic wave gauge - various limitations


