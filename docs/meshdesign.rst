.. _meshdesign:

Mesh Design
===========


Meshing Tools
--------------


BlueKenue 
~~~~~~~~~~


QMesh (QGIS+GMesh)
~~~~~~~~~~~~~~~~~~~


Basic Mesh Structure
---------------------


Domain Constraints
~~~~~~~~~~~~~~~~~~~


Resolution Limits
~~~~~~~~~~~~~~~~~~


Number of Model Nodes
~~~~~~~~~~~~~~~~~~~~~~


Mesh Refinement
----------------

Bathymetry Based
~~~~~~~~~~~~~~~~~


Distance Based
~~~~~~~~~~~~~~~


Dynamics Based
~~~~~~~~~~~~~~~



