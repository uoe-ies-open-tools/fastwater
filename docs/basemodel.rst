.. _basemodel:

Base Model
==========
The purpose of the Base Model is to provide a base starting point for regional modelling of the Orkneys extending far eonough off-shore to support wave modelling. This base model mesh can be refined in areas of interest to meet different end-use requirements. The velocity data generated will form a base set of velocity field data that can be used as input to a one-way wave-current coupled model.

Specification
#############
The aim of the base model construct is to build a regional model that meets the minimum recommended requirements for application to feasibility studies and to have sufficient data resolution to provide useful input velocity data for regional wave models.

The focus region for this project is the key Northern Scottish waters where ORE developement is currently (as of July 2022) predominantly focussed. The model domain must cover all of the Orkney Islands, the northwest region of the Scottish mainland, and needs to extend sufficently far from coastal areas to allow a good representation of the tidal forcing from the tidal atlas data, and to provide sufficient fetch for wind driven waves.

The spatial resolution in tidal channels and near-shore wave energy sites should be close to the resolution of 200m as recommended by IEC/TS 62600-201:215 for feasibility studies.

Bathymetry data should be at a spatial resolution that is the same as or finer than  that of the target channel resolution.

Spatially varying bottom friction should be included to capture this effects of varying bottom roughness.

The open boundary tidal forcing must include the dominanat tidal constituents and their corresponding shallow-water harmonics.

The model will be run in 3D to capture the complex spatial variability in the flow structures through channels, around islands, and past headlands, i.e. ares where flow acceleration leads to increased available kinetic energy for extraction.


Domain Defintion
################
The base model domain regions covers the NW of Scotland, the Orkney Island and the Fair Isles, effectively -5.25E to -0.9E, and 58.1N to 55.86N. This provides an outer boundary sufficiently far from the areas of interest to allow good representation of the tidal forcing and to allow a reasonable fetch for wind driven processes. The smaller outer islands have been included as these will impact on wave propagation when modelling waves.

Bathymetry
~~~~~~~~~~
Bathymetry data taken from EMODNET server. Use merged bathymetry and topography data extracted from the web portal using download by user defined area and saving as a geotif. This needs to be done in sections to get the required coverage as the is t limit to the size of a single file request. The extracted geotifs are merged in qgis and cropped the give a clean coverage with no missing data areas. These data have been found to be offset by a single pixel (+1 pixel in x direction, -1 pixel in y direction). A correction is applied by using the rasterio python package to translate the data. The bathy data are given as Astronomical MLT, but we need MSL for the model. Tidal range data are available from the UKHO, in the first instance a mean area offset is taken as half the mean tidal range over the area of interest. This offset is added using the rasterio python tools. The corrected geotif is then converted to a netcdf file (using the export function in QGIS) for use with the qmesh tools, and to an \*.XYZ file (using the gdal2xyz tool in QGIS) for use with the BlueKenue tools.

Bottom Friction
~~~~~~~~~~~~~~~
The bottom friction is defined using 2019 EMODNET Seabed Habitats type data. These data are in the form of a shapefile that provides a substrate class name. These classes can be mapped to a set of C_100 drag coefficient. The C_100 drag coefficient can then be mapped onto the model mesh nodes so that a range of frictions coefficients (including depth dependent) can be calculated appropriately.

The class mappings are as follows:

* Fix geometries of EUSM2019 shapefiles using QGIS tool Vector geometry > Fix geometries
* Export shapefile to change CRS to EPSG:32630
* Clip to extent of bathymetry file using QGIS tool Vectory overlay > Clip
* Manually set drag coefficient value by substrate class name
* Add a geospatial index to both the bathymetry and substrate shape files open layer properties thne go to source - Create spatial index
* Join the substrate data to the bathy data using the QGIS tool Vector general > Joint attributes by location. The base layer is the bathymetry points shapefile and teh overlay layer is the substrate polygons shapefile. Select the Class and Cf fields to join, and join using touches and within, and discard points where there is no match.
* Use python script to add the range of friction coefficients fields 

These are based on data from ....

Coastlines
~~~~~~~~~~

Generate coastlines in QGIS - hand digitization
Convert the coasltine data to simplified fixed lengths - use QGIS Vector>Geometry Tools>Simplify, tolerance = length, set Simplification method to Area (Visvalingam)
Generated 5m, 10m, 25m, 50m, 100m, 150m, 250m, 500m resolutions
QMesh requires a minimum of two input shapefiles: 

1) outlines.shp - all interal lines (islands as closed loops) and bounding lines (separate open boundary  and coastal boundary lines)
2) resolutions_lines.shp - contains lines that are used to expand the mesh from (islands as closed loops, coastal boundary lines, and any internal lines used to control growth)

Multiple rasters at diffrent resolutions can be gerenated and merged (min or max) to furhter constrain where resolutino is increased, the various colastline resolution files can be used to apply these constraints.

Issues - when loading raster from file need to sue the correct reading function - e.g. readNetCDF for netCDF files...

Pass East/North translation factors and a constant vertical datum shift to give mean correction for difference between AMLT and MSL.

Mesh Generation
###############

OpenTelemac Construct
#####################


