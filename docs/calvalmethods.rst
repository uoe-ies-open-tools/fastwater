.. _calvalmethods:

Cal/Val Methodology
===================


Overview
########

For cal/val purposes a very high resolution 3-D hydrodynamic process model is constructed to 
represent the "truth" that will be used to test different model (2-D and 3-D) constructs and 
configurations against. More generally this will be extended to observational data for 
"real-world" systems, once the methodology has been defined.

Simplfied Process Models
########################

Headland Model
^^^^^^^^^^^^^^
The simplified process model used to develop the GPM/Bayes cal/val methodology is based on the 
headland model used by `Stansby et al., (2016)`_. Initially this will be run with a constant 
through flow along the channel. This constant flow generates a trapped eddy down-stream of the 
headland, and after the spin-up period an instability forms along the shear layer interface 
between the trapped eddy and the free-stream flow region, resulting in an eddy shedding process.



.. _Stansby et al., (2016): https://doi.org/10.1016/j.coastaleng.2016.05.008


Input Parameters and Cal/Val Metrics
####################################


Gaussian Process Model - Fast Simulation
########################################



Bayesian Cal/Val
################


