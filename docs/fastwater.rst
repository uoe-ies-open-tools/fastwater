fastwater package
=================

.. automodule:: fastwater
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   fastwater.analysis
   fastwater.general
   fastwater.otm_gpl
   fastwater.qmesh
   fastwater.telemac
