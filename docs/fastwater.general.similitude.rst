fastwater.general.similitude module
===================================

.. automodule:: fastwater.general.similitude
   :members:
   :undoc-members:
   :show-inheritance:
