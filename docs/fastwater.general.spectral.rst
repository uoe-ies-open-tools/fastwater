fastwater.general.spectral module
=================================

.. automodule:: fastwater.general.spectral
   :members:
   :undoc-members:
   :show-inheritance:
