fastwater.analysis.tidal module
===============================

.. automodule:: fastwater.analysis.tidal
   :members:
   :undoc-members:
   :show-inheritance:
