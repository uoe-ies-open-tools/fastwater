fastwater.analysis package
==========================

.. automodule:: fastwater.analysis
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   fastwater.analysis.dataReaders
   fastwater.analysis.flowChar
   fastwater.analysis.graphics
   fastwater.analysis.tecSpecs
   fastwater.analysis.tidal
   fastwater.analysis.validation
   fastwater.analysis.xtrackReader
