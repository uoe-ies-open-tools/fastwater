fastwater.general package
=========================

.. automodule:: fastwater.general
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   fastwater.general.fileTools
   fastwater.general.geometry
   fastwater.general.meshTools
   fastwater.general.physics
   fastwater.general.similitude
   fastwater.general.spectral
   fastwater.general.statsTools
   fastwater.general.substrates
   fastwater.general.timeFuncs
