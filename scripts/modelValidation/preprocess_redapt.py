# -*- coding: utf-8 -*-
"""
Example Script: Apply pre-processing to extracted location specific timeseries 
                from the FASTWATER FoW models.

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
import scipy.io as sio
# Local Module Dependencies
from fastwater.general.geometry import rotateVectorField
from fastwater.analysis.flowChar import extract_redapt_ts_at_height
from fastwater.analysis.flowChar import preprocess_ts, dateStr, dateNumFromStr
from fastwater.analysis.graphics import flow_class_diagram
from fastwater.analysis.graphics import speed_bin_histograms
from fastwater.analysis.graphics import directional_spread_plot
from fastwater.analysis.tecSpecs import tecD10, tecDeepGenIV, tecOrbitalO2
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

datapath = 'E:/FASTWATER/data/ReDAPT/NETCDF'
datasrc = 'ReDAPT'

#dataset = 'ADCPTD7_01_Dep1'
#srchstr = 'TD7_01'
#outfile = 'redapt_td701.mat'
#dhead = 13.2
dataset = 'ADCPTD7_02_Dep1'
srchstr = 'TD7_02'
outfile = 'redapt_td702.mat'
dhead = None

#tec = tecD10()
tec = tecDeepGenIV()
#tec = tecOrbitalO2()

hab = tec.bottomMounted
zhub = tec.hubHeight
rdia = tec.rotorDiam
Cp = tec.Cp

#smthw = 300
#subw = 300.0
#of_suffix = '.'
smthw = 60
subw = 60.0
#of_suffix = '_win01min.'
#of_suffix = '_win01min_7day.'
of_suffix = '_win01min_14days.'

ts_obs = extract_redapt_ts_at_height(datapath,srchstr,dataset,datasrc,zhub,hab=hab,smth_win=smthw,sub_win=subw)
if dhead is not None:
    u = ts_obs['EAST']
    v = ts_obs['NORTH']
    w = np.zeros(u.shape)
    ur, vr, wr = rotateVectorField(u, v, w, -dhead)

    ts_obs['EAST'] = ur.copy()
    ts_obs['NORTH'] = vr.copy()

#data_obs = preprocess_ts(ts_obs,tec=tec)
tstart = dateNumFromStr('2014-09-21 00:00:00')
data_obs = preprocess_ts(ts_obs,tec=tec,ndays=14,tstart=tstart)
fh_obs = flow_class_diagram(data_obs,tec=tec,velmax=4.5)
fh_spdh = speed_bin_histograms(data_obs,tec=tec,binwidth=0.1,maxfrac=0.08,maxspd=4.5)
fh_dsrd = directional_spread_plot(data_obs)


ofprts = outfile.split('.')
if hab:
    rstr = '_hab_'
else:
    rstr = '_dbs_'

ofile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m_TS'+of_suffix+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,ts_obs)

ofile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m'+of_suffix+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,data_obs)


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

