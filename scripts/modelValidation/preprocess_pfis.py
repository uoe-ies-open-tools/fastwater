# -*- coding: utf-8 -*-
"""
Created on Wed Aug  3 09:31:03 2022

@author: cold2
"""
# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
# Local Module Dependencies
from fastwater.analysis.flowChar import extract_model_ts_at_height
from fastwater.analysis.flowChar import extract_model_depthavg
from fastwater.analysis.flowChar import extract_model_rotav
from fastwater.analysis.flowChar import preprocess_ts
from fastwater.analysis.flowChar import streamwise_flow, tec_power
from fastwater.analysis.graphics import flow_class_diagram
from fastwater.analysis.graphics import speed_bin_histograms
from fastwater.analysis.graphics import directional_spread_plot
from fastwater.analysis.tecSpecs import tecAR2000
from fastwater.general.timeFuncs import dateStr, dateNumFromStr
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

# Load TEC data
tec = tecAR2000()
hab = tec.bottomMounted
zhub = tec.hubHeight
rdia = tec.rotorDiam
Cp = tec.Cp

# General file information
datapath = 'E:/FASTWATER/telemac3d/fw_pfis_case_study/EXTRACT'
datasrc = 'FW PFIS MODEL (D2C Refine)'
outprefix = 'pfis_d2c_model'

# TEC Location 1
srchstr = 'TEC1'
dataset = 'MeyGen TEC1'
outfile = outprefix+'_tec1.mat'

ts_mod = extract_model_ts_at_height(datapath,srchstr,dataset,datasrc,zhub,hab=hab)
data_mod = preprocess_ts(ts_mod,tec=tec)
fh_mod = flow_class_diagram(data_mod,tec=tec,velmax=5.5)

fh_spdh = speed_bin_histograms(data_mod,tec=tec,binwidth=0.1,maxfrac=0.06,maxspd=5.5)
fh_dsrd = directional_spread_plot(data_mod)

pwra_tec1 = extract_model_rotav(datapath,srchstr,dataset,datasrc,data_mod,rdia,zhub,hab=True,pwra=True,Cp=Cp)

ofprts = outfile.split('.')
if hab:
    rstr = '_hab_'
else:
    rstr = '_dbs_'

ofile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m_TS.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,ts_mod)

ofile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,data_mod)


# TEC Location 2
srchstr = 'TEC2'
dataset = 'MeyGen TEC2'
outfile = outprefix+'_tec2.mat'

ts_mod = extract_model_ts_at_height(datapath,srchstr,dataset,datasrc,zhub,hab=hab)
data_mod = preprocess_ts(ts_mod,tec=tec)
fh_mod = flow_class_diagram(data_mod,tec=tec,velmax=5.5)

fh_spdh = speed_bin_histograms(data_mod,tec=tec,binwidth=0.1,maxfrac=0.06,maxspd=5.5)
fh_dsrd = directional_spread_plot(data_mod)

pwra_tec2 = extract_model_rotav(datapath,srchstr,dataset,datasrc,data_mod,rdia,zhub,hab=True,pwra=True,Cp=Cp)

ofprts = outfile.split('.')
if hab:
    rstr = '_hab_'
else:
    rstr = '_dbs_'

ofile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m_TS.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,ts_mod)

ofile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,data_mod)


# Compare Power based on PWRA

t = pwra_tec1['timestamp']+dateNumFromStr(pwra_tec1['epoch'].replace('/','-'))
t0 = np.floor(t[0])
epstr = dateStr(t0)

fig = plt.figure(figsize=(12,8))
axs=fig.subplots(2,1)
dP = pwra_tec2['tec_pwr']/pwra_tec1['tec_pwr']
axs[0].plot(t-t0,pwra_tec1['tec_pwr']/1.0e6,label='TEC 1')
axs[0].plot(t-t0,pwra_tec2['tec_pwr']/1.0e6,label='TEC 2')
axs[0].set_xlim([0.0,27.0])
axs[0].set_ylim([0.0,6.0])
axs[0].grid('on')
axs[0].legend()
axs[0].set_ylabel('TEC Power  [ MW ]',fontsize=8)
axs[0].set_title('Comparison of Estimated Power Production by TEC1 and TEC2 Turbines')
axs[0].set_position([0.075,0.56,0.88,0.37])

dP = pwra_tec2['tec_pwr']-pwra_tec1['tec_pwr']
axs[1].plot(t-t0,dP/1.0e6)
axs[1].set_xlim([0.0,27.0])
axs[1].set_ylim([-1.0,0.0])
axs[1].grid('on')
axs[1].set_xlabel('Timestamp  [days from '+epstr+']',fontsize=8)
axs[1].set_ylabel('TEC2-TEC1 Power Differece  [ MW ]',fontsize=8)
axs[1].set_title('Difference in Estimated Power Production by TEC1 and TEC2 Turbines')
axs[1].set_position([0.075,0.09,0.88,0.37])

'''
plt.figure(figsize=(9,9))
plt.plot(pwra_tec1['tec_pwr']/1.0e6,pwra_tec2['tec_pwr']/1.0e6,'.')
ax = plt.gca()
ax.set_aspect('equal')
plt.grid('on')
plt.xlabel('TEC 1 Power  [ MW ]')
plt.ylabel('TEC 2 Power  [ MW ]')
'''

# Energy yield difference
energy_tec1 = np.nansum(pwra_tec1['tec_pwr'])*300.0
energy_tec2 = np.nansum(pwra_tec2['tec_pwr'])*300.0

yield_difference = 100.*energy_tec2/energy_tec1

print('Energy Yield Difference: TEC 2 yield is '+"{0:+.1f}".format(yield_difference)+'% of TEC 1')

# Exclude speeds below TEC cut-in
tec1indx = np.where(pwra_tec1['vel_rot'] < tec.cutInSpeed)[0]
tec1_pwr = pwra_tec1['tec_pwr'].copy()
tec1_pwr[tec1indx] = np.nan
energy_tec1_limited = np.nansum(tec1_pwr)*300.0

tec2indx = np.where(pwra_tec2['vel_rot'] < tec.cutInSpeed)[0]
tec2_pwr = pwra_tec2['tec_pwr'].copy()
tec2_pwr[tec2indx] = np.nan
energy_tec2_limited = np.nansum(tec2_pwr)*300.0

yield_difference_limited = 100.*energy_tec2_limited/energy_tec1_limited

print('Energy Yield Difference (above cut-in speed): TEC 2 yield is '+"{0:+.1f}".format(yield_difference_limited)+'% of TEC 1')

#=============================================
# Depth-Averaged Velocity Anaysis
#=============================================

# TEC Location 1
srchstr = 'TEC1'
dataset = 'MeyGen TEC1'
outfile = outprefix+'_tec1.mat'

ts_da_mod = extract_model_depthavg(datapath,srchstr,dataset,datasrc,zhub,hab=hab)
data_da_mod = preprocess_ts(ts_da_mod,tec=tec)
fh_da_mod = flow_class_diagram(data_da_mod,tec=tec,velmax=5.5)

fh_da_spdh = speed_bin_histograms(data_da_mod,tec=tec,binwidth=0.1,maxfrac=0.06,maxspd=5.5)
fh_da_dsrd = directional_spread_plot(data_da_mod)

vel_strm = streamwise_flow(data_da_mod)
pwr_da_tec1 = tec_power(vel_strm,rdia,Cp=Cp)

ofprts = outfile.split('.')
if hab:
    rstr = '_hab_'
else:
    rstr = '_dbs_'

ofile = ofprts[0]+rstr+'_depth_avg_TS.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,ts_da_mod)

ofile = ofprts[0]+'_depth_avg.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,data_da_mod)


# TEC Location 2
srchstr = 'TEC2'
dataset = 'MeyGen TEC2'
outfile = outprefix+'_tec2.mat'

ts_da_mod = extract_model_depthavg(datapath,srchstr,dataset,datasrc,zhub,hab=hab)
data_da_mod = preprocess_ts(ts_da_mod,tec=tec)
fh_da_mod = flow_class_diagram(data_da_mod,tec=tec,velmax=5.5)

fh_da_spdh = speed_bin_histograms(data_da_mod,tec=tec,binwidth=0.1,maxfrac=0.06,maxspd=5.5)
fh_da_dsrd = directional_spread_plot(data_da_mod)

vel_strm = streamwise_flow(data_da_mod)
pwr_da_tec2 = tec_power(vel_strm,rdia,Cp=Cp)

ofprts = outfile.split('.')
if hab:
    rstr = '_hab_'
else:
    rstr = '_dbs_'

ofile = ofprts[0]+rstr+'_depth_avg_TS.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,ts_da_mod)

ofile = ofprts[0]+'_depth_avg.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,data_da_mod)


# Compare Power based on depth average velocity power

t = data_da_mod['time']+dateNumFromStr(data_da_mod['epoch'].replace('/','-'))
t0 = np.floor(t[0])
epstr = dateStr(t0)

fig = plt.figure(figsize=(12,8))
axs=fig.subplots(2,1)
dP = pwr_da_tec2/pwr_da_tec1
axs[0].plot(t-t0,pwr_da_tec1/1.0e6,label='TEC 1')
axs[0].plot(t-t0,pwr_da_tec2/1.0e6,label='TEC 2')
axs[0].set_xlim([0.0,27.0])
axs[0].set_ylim([0.0,6.0])
axs[0].grid('on')
axs[0].legend()
axs[0].set_ylabel('TEC Power  [ MW ]',fontsize=8)
axs[0].set_title('Comparison of Estimated Power Production by TEC1 and TEC2 Turbines (Depth Averaged Velocity)')
axs[0].set_position([0.075,0.56,0.88,0.37])

dP = pwr_da_tec2-pwr_da_tec1
axs[1].plot(t-t0,dP/1.0e6)
axs[1].set_xlim([0.0,27.0])
axs[1].set_ylim([-1.0,0.0])
axs[1].grid('on')
axs[1].set_xlabel('Timestamp  [days from '+epstr+']',fontsize=8)
axs[1].set_ylabel('TEC2-TEC1 Power Differece  [ MW ]',fontsize=8)
axs[1].set_title('Difference in Estimated Power Production by TEC1 and TEC2 Turbines (Depth Averaged)')
axs[1].set_position([0.075,0.09,0.88,0.37])

# Energy yield difference
energy_da_tec1 = np.nansum(pwr_da_tec1)*300.0
energy_da_tec2 = np.nansum(pwr_da_tec2)*300.0

yield_difference_da = 100.*energy_da_tec2/energy_da_tec1
print('Depth-Averaged velocity analysis')
print('Energy Yield Difference: TEC 2 yield is '+"{0:+.1f}".format(yield_difference_da)+'% of TEC 1')

# Exclude speeds below TEC cut-in
tec1indx = np.where(pwr_da_tec1 < tec.cutInSpeed)[0]
tec1_da_pwr = pwr_da_tec1.copy()
tec1_da_pwr[tec1indx] = np.nan
energy_da_tec1_limited = np.nansum(tec1_da_pwr)*300.0

tec2indx = np.where(pwr_da_tec2 < tec.cutInSpeed)[0]
tec2_da_pwr = pwr_da_tec2.copy()
tec2_da_pwr[tec2indx] = np.nan
energy_da_tec2_limited = np.nansum(tec2_da_pwr)*300.0

yield_difference_limited_da = 100.*energy_da_tec2_limited/energy_da_tec1_limited

print('Energy Yield Difference (above cut-in speed): TEC 2 yield is '+"{0:+.1f}".format(yield_difference_limited_da)+'% of TEC 1')

fig = plt.figure(figsize=(12,4))
plt.plot(t-t0,pwr_da_tec1/1.0e6,label='TEC 1')
plt.plot(t-t0,pwr_da_tec2/1.0e6,label='TEC 2')
ax = plt.gca()
ax.set_xlim([0.0,27.0])
ax.set_ylim([0.0,6.0])
ax.tick_params(axis='both', labelsize=8)
ax.set_yticklabels([])
ax.grid('on')
plt.legend(fontsize=8)
plt.xlabel('Timestamp  [days from '+epstr+']',fontsize=8)
plt.ylabel('TEC Power  [ MW ]',fontsize=8)
plt.title('Comparison of Estimated Power Production by TEC1 and TEC2 Turbines (Depth Averaged Velocity)',fontsize=10)
ax.set_position([0.05,0.14,0.92,0.77])





#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

