# -*- coding: utf-8 -*-
"""
Example Script: Apply pre-processing to extracted timeseries from archived
                ReDAPT ADCP data.

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
import scipy.io as sio
# Local Module Dependencies
from fastwater.analysis.flowChar import extract_model_depthavg
from fastwater.analysis.flowChar import extract_model_ts_at_height
from fastwater.analysis.flowChar import preprocess_ts
from fastwater.analysis.graphics import flow_class_diagram
from fastwater.analysis.graphics import speed_bin_histograms
from fastwater.analysis.graphics import directional_spread_plot
from fastwater.analysis.tecSpecs import tecD10, tecDeepGenIV, tecOrbitalO2
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

#datapath = 'E:/FASTWATER/telemac3d/fw_base_model/EXTRACT'
#datasrc = 'FW BASE MODEL'
#outprefix = 'fw_base_model'
##srchstr = 'TD7_01'
#srchstr = 'TD7_02'

#datapath = 'E:/FASTWATER/telemac3d/fw_fow_case_study/EXTRACT'
#datasrc = 'FW FoW MODEL (D2C Refine)'
#outprefix = 'fow_d2c_model'
##srchstr = '*d2c*TD7_01'
#srchstr = '*d2c*TD7_02'

#datapath = 'E:/FASTWATER/telemac3d/fw_fow_case_study/EXTRACT'
#datasrc = 'FW FoW MODEL (Vort Refine)'
#outprefix = 'fow_vort_model'
##srchstr = '*vort*TD7_01'
#srchstr = '*vort*TD7_02'

datapath = 'E:/FASTWATER/telemac3d/fw_fow_case_study_v02/EXTRACT'
datasrc = 'FW FoW MODEL (Vort Refine)'
outprefix = 'fow_vort_model_v02'
#srchstr = '*vort*TD7_01'
srchstr = '*vort*TD7_02'

#dataset = 'ADCPTD7_01_Dep1'
#titlestr = 'Model-Obs comparison - ReDAPT ADCP TD7_01'
#outfile = outprefix+'_td701.mat'
dataset = 'ADCPTD7_02_Dep1'
titlestr = 'Model-Obs comparison - ReDAPT ADCP TD7_02'
outfile = outprefix+'_td702.mat'

#tec = tecD10()
tec = tecDeepGenIV()
#tec = tecOrbitalO2()

hab = tec.bottomMounted
zhub = tec.hubHeight
rdia = tec.rotorDiam
Cp = tec.Cp

#ts_da_mod = extract_model_depthavg(datapath,srchstr,dataset,datasrc,zhub,hab=hab)
#data_da_mod = preprocess_ts(ts_da_mod)
#fh_da_mod = flow_class_diagram(data_da_mod)

ts_mod = extract_model_ts_at_height(datapath,srchstr,dataset,datasrc,zhub,hab=hab)
data_mod = preprocess_ts(ts_mod,tec=tec)
fh_mod = flow_class_diagram(data_mod,tec=tec,velmax=4.5)
fh_spdh = speed_bin_histograms(data_mod,tec=tec,binwidth=0.1,maxfrac=0.08,maxspd=4.5)
fh_dsrd = directional_spread_plot(data_mod)

ofprts = outfile.split('.')
if hab:
    rstr = '_hab_'
else:
    rstr = '_dbs_'

ofile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m_TS.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,ts_mod)

ofile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m.'+ofprts[1]
sio.savemat('E:/FASTWATER/validation/'+ofile,data_mod)


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

