# -*- coding: utf-8 -*-
"""
Example Script:  Validation of the FASTWATER FoW models against RaDAPT ADCP data


"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from fastwater.analysis.tecSpecs import tecD10, tecDeepGenIV, tecOrbitalO2
from fastwater.analysis.validation import validateFlowParameter
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

datapath = 'E:/FASTWATER/validation'

#model_prefix = 'fw_base_model'
#model_prefix = 'fow_d2c_model'
#model_prefix = 'fow_vort_model'
model_prefix = 'fow_vort_model_v02'

#dataset = 'ADCPTD7_01_Dep1'
#datasuffix = 'td701.mat'

dataset = 'ADCPTD7_02_Dep1'
datasuffix = 'td702.mat'

datfile = model_prefix+'_'+datasuffix

#tec = tecD10()
tec = tecDeepGenIV()
#tec = tecOrbitalO2()

hab = tec.bottomMounted
zhub = tec.hubHeight
rdia = tec.rotorDiam
Cp = tec.Cp

ofprts = datfile.split('.')
if hab:
    rstr = '_hab_'
else:
    rstr = '_dbs_'

modfile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m.'+ofprts[1]
# Load pre-processed data
modFile = datapath+'/'+modfile

datfile = 'redapt'+'_'+datasuffix
ofprts = datfile.split('.')
if hab:
    rstr = '_hab_'
else:
    rstr = '_dbs_'
if 'vort' in model_prefix:
    #obsfile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m_win01min.'+ofprts[1]
    obsfile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m_win01min_14days.'+ofprts[1]
else:
    obsfile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m.'+ofprts[1]
obsFile = datapath+'/'+obsfile

# Calculate Validataion Statistics
metrics, figs = validateFlowParameter(modFile,obsFile,['magnitude','direction'])


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

