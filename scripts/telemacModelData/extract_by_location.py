#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 16 06:11:33 2021

@author: cold2
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import sys
import getopt
import numpy as np
from datetime import datetime,timedelta
# Non-Standard Python Dependencies
# Local Module Dependencies
from fastwater.general.fileTools import getListOfFiles
from fastwater.telemac import extraction as xtrct
# Other Dependencies

# ----------------------------------------------------------------------------
#   GLOBAL CONSTANTS
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
#-----------------------------------------------------------------------------

def initialise_run(runType,fileName):
    # Parser input file for run parameters
    if 'loc' in runType:
        runParams = xtrct.parseLocsRunFile(fileName)
    elif 'nod' in runType:
        runParams = xtrct.parseNodesRunFile(fileName)
    # Get list of files to process
    files = getListOfFiles(runParams['dataPath'],runParams['srchStr'])
    # Set start and stop dates for extraction
    startDT = datetime.strptime(runParams['startDate'],'%d-%b-%Y')
    stopDT = startDT + timedelta(days=runParams['nDays'])
    startDate = int(datetime.strftime(startDT,'%Y%m%d'))
    stopDate = int(datetime.strftime(stopDT,'%Y%m%d'))
    
    return runParams,files,startDate,stopDate

def process_nodes(fileName):
    # Initialise run
    runParams,files,startDate,stopDate = initialise_run('node',fileName)
    
    modelVer = '01'
    procStr = 'Node Data'
    idStr = "_NodeSet01"
    
    # Loop over all files
    for runFile in files:
        
        runStr = runFile[0:-7]
        dateval = int(runStr.split('_')[-1])
        
        if (dateval >= startDate) & (dateval < stopDate):
            nodes = runParams['nodes']
            # process all selected nodes for current data file
            xtrct.processNodeDataFile(runParams['dataPath'],runStr,idStr,modelVer,procStr,nodes,showbar=False)
            
    return

def process_locations(fileName):
    # Initialise run
    runParams,files,startDate,stopDate = initialise_run('location',fileName)
    
    modelVer = '01'
    procStr = 'Location Data'
        
    # Loop over all files
    for runFile in files:
        
        runStr = runFile[0:-7]
        dateval = int(runStr.split('_')[-1])
        if (dateval >= startDate) & (dateval < stopDate):
            locs = np.zeros([runParams['nLocs'],2])
            locs[:,0] = runParams['xLoc']
            locs[:,1] = runParams['yLoc']
            locIDs = runParams['locID']
            # process all locations for current data file
            xtrct.processLocsDataFile(runParams['dataPath'],runStr,locIDs,modelVer,procStr,locs,showbar=False)
        
    return

def process_roi(fileName):
    return

# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------
def main(argv):

    try:
        opts, args = getopt.getopt(argv,"hr:f:",["--runtype","--fname"])
    except getopt.GetoptError:
        print('python3 extract_orkbase.py -r <runtype> -f <fname>')
        sys.exit(2)
    if opts == []:
        print('python3 extract_orkbase.py -r <runtype> -f <fname>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('python3 extract_orkbase.py -r <runtype> -f <fname>')
            sys.exit(2)
        elif opt in ("-r","--runtype"):
            runType = arg
        elif opt in ("-f","--fname"):
            fileName = arg
    
    if 'loc' in runType.lower():
        process_locations(fileName)
    elif 'nod' in runType.lower():
        process_nodes(fileName)
    elif 'roi' in runType.lower():
        process_roi(fileName)

    print('Process complete...')

    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])

