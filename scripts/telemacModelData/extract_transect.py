#!/usr/bin/env python3

import sys
import getopt
import scipy.io as sio
from fastwater.telemac.extraction import parseTransectRunFile
from fastwater.telemac.extraction import extractTransect

#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

def main(argv):

    try:
        opts, args = getopt.getopt(argv,"hf:",["--fname"])
    except getopt.GetoptError:
        print('./extract_transect.py -f <fname>')
        sys.exit(2)
    if opts == []:
        print('./extract_transect.py -f <fname>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('./extract_transect.py -f <fname>')
            sys.exit(2)
        elif opt in ("-f","--fname"):
            fileName = arg
    

    print('Process complete...')
    
    params = parseTransectRunFile(fileName)

    print('  MODEL_'+params['runStr']+'  '+params['titleStr'])
    trnsct = extractTransect(params['dataPath'],params['runStr'],
                             params['titleStr'],params['loc01'],
                             params['loc02'],params['dL'],False)
    
    sio.savemat(params['outPath']+'/'+params['outFile'],trnsct)
    
    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])
