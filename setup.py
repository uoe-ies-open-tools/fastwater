import setuptools
from setuptools import setup

setup(name='fastwater',
      version='0.0b0',
      description='Freely Available Simulation Toolset for Waves, Tides and Eddy Replication',
      url='https://git.ecdf.ed.ac.uk/uoe-ies-open-tools/fastwater',
      author='Chris Old',
      author_email='',
      license='BSD',
      packages=setuptools.find_packages(),
      zip_safe=False,
      python_requires='>=3.7',
      install_requires=[
            'numpy',
            'pytictoc',
            'pytides',
            'scikit-learn',
            'utm',
            'metpy',
            'meshio',
            'netcdf4',
            'matplotlib',
            'scipy',
            ]
      )
